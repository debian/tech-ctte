#+TITLE: Early invocation

* the proposal:

Allow the TC to be invoked early
--------------------------------

The requirement to make decisions as a measure of last resort means that by the
time the committee is called to action, most issues have already become a
flamewar where no matter the result people will end up unhappy. Removing this
requirement would allow the TC to get involved earlier, helping developers find
consensus rather than beating them with a stick.

For this change to be successful, there should be a clear way of invoking the
TC that doesn't imply a "nuclear option". A way of asking for help solving a
technical disagreement without generating resentment.

Developers don't always know how to solve problems before bringing them to the
TC and that's ok. If we can find a way to let the TC help with conflict without
creating "winners" and "losers" we could make it a much more useful body.

**Proposal 2**: Modify the Constitution to allow the TC to get invoked early,
clarifying how that works.

* relevant bits of the constitution:

6.3.6. Technical Committee makes decisions only as last resort.

   The Technical Committee does not make a technical decision until efforts to
   resolve it via consensus have been tried and failed, unless it has been asked
   to make a decision by the person or body who would normally be responsible
   for it.

6.1.5. Offer advice.

   The Technical Committee may make formal announcements about its views on any
   matter. _Individual members may of course make informal statements about
   their views and about the likely views of the committee._

* thoughts arising:

While the 6.3.6. section heading includes "only as last resort" which certainly
gives the impression that one should not involve the TC until the fire is
properly blazing, there is already the possibility of emphasising 6.1.5. as an
option to approach the TC earlier.

Does this require a constitutional amendment, or just better documentation of
the existing options?

If the constitution were to be amended, would this to be to allow the TC to
actually rule earlier, when all options have not yet been explored? If so, how
do we avoid people who are familiar with the TC processes deciding to jump
straight to invoking the TC because they cannot be bothered to put the effort
into trying to solve the issue themselves.

If this is to be done without constitutional amendment, how? Would it be OK to
have an occasional announcement mail remind people that they can always send
mails, either in private or public to the committee, asking for attention to be
paid to a bug or mailing-list thread?

While the TC is prohibited from diving in and ruling on such an ongoing
situation, I don't see anything to stop it being discussed in a meeting, and if
it is clear where the consensus might lay if a ruling were required, that might
be provided as a data point to inform the ongoing discussion.

Of course, the idea that the TC can respond in a timely manner to an ongoing
discussion may well be nonsense, but I suspect that unburdened by the need to
come to a consensus, individual members of the TC would manage it often enough
to make it worthwhile if more of these cases were hightlighted earlier in their
lifecycle.

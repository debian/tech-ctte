Phil Hands's recruitment scripts
=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

ctte-cronscript.sh is a copy of the script that is (probably) being run once a
month via spwhitton's crontab on master.d.o. At the time of writing it was set
to run on the 16th of the month.

It picks people at random that have yet to receive such a mail, and sends them a
mail asking them to think about nominating someone.  The script has comments.

The number of mails sent is determined by grabbing /this/ file from the
tech-ctte repo on salsa.d.o and looking for 'MAILS_TO_SEND=<number>' on a line
by itself, (as seen below). There is an upper limit (50 at the time of writing).

If you want to pause sending of these emails, just set the number to 0 (zero)
and then push the change to salsa so that the cron script gets to see the change:

################

MAILS_TO_SEND=10

################

Miscellaneous notes:

Also in this directory is the old 'bash_profile' file, that describes how I
(Phil) used to run this stuff by hand, and ctte-nominators.txt which is the
template for the mail to be sent that I used.

IIRC I generated that template by sending a signed mail to myself, and then
editing the headers to have the appropriate patterns. Alternatively,
roehling wrote a Python script create-ctte-nominations-template.py that you
can use.

If you want to set this up for yourself elsewhere, you'll probably want to
grab a copy of master:~roehling/local/ctte-dontspam to ensure that you don't
send mails to people that have already had one, and you'll also want to get
roehling to turn his cron off before you turn yours on.

Here's what's currently in roehling's crontab:

  35 15 16 * *   ~/src/tech-ctte/scripts/recruitment/ctte-cronscript.sh

Cheers, Phil.


From: Philip Hands <phil@hands.com>
To: %VICTIM%@debian.org
Subject: Encouraging nominations to the technical committee
Date: %DATE%
MIME-Version: 1.0
Content-Type: multipart/signed; boundary="=-=-=";
	micalg=pgp-sha512; protocol="application/pgp-signature"

--=-=-=
Content-Type: text/plain
Content-Transfer-Encoding: quoted-printable

Hi,

As part of the push to get more nominations for the technical committee,
I'm sending out a few invitations to nominate people, and this is one.

You don't need an invitation to nominate people, but I'm hoping that by
doing this I'll prompt you to consider the people you know well in the
project, and so encourage you to suggest good candidates that might not
have already been thought of.

For instance, if you have personal experience of someone as being a wise
head and/or who has a track record of calming fraught situations (even
if they might be mostly unknown outside whatever niche of the project
that you both have in common) please take this as a request to nominate
them.

Nominations should be sent to:  debian-ctte-private@debian.org

Just send their name if that's all you have time for, although something
describing why you thought of them would be nice too, and if you have
time to dig out a reference or two to illustrate, that would be great.

There's no need to worry -- we ask them if they're willing, so you're
not forcing anyone to do anything.  Also, we won't publish you name, or
tell the nominee who nominated them.  People are generally very pleased
to have been suggested, even if they decide to decline the nomination.

I hope you don't mind the intrusion, but if you do please tell me that,
so that I can either improve or abandon this experiment as appropriate.

Cheers, Phil.

P.S. If you're wondering:  "Why is Phil asking me? He knows I ..."
Well, TBH, I didn't.  You were chosen by something that boils down to:

  list_all_developers_not_yet_asked | shuf -n 10

If this has resulted in you getting a mail that strikes you as
pointless, please accept my apologies -- it won't happen again.
=2D-=20
|)|  Philip Hands  [+44 (0)20 8530 9560]  HANDS.COM Ltd.
|-|  http://www.hands.com/    http://ftp.uk.debian.org/
|(|  Hugo-Klemm-Strasse 34,   21075 Hamburg,    GERMANY

--=-=-=
Content-Type: application/pgp-signature; name="signature.asc"

-----BEGIN PGP SIGNATURE-----

iQIzBAEBCgAdFiEE3/FBWs4yJ/zyBwfW0EujoAEl1cAFAlpDsV8ACgkQ0EujoAEl
[                                                              ]
[                                                              ]
[   disarmed PGP signature, left as an example for the repo.   ]
[                                                              ]
[                                                              ]
TFvoVARaGby4+Z0B7UomwRegyfNDNBjXPE8PJVg+i/tJQ+WuvPzFYCt0DGWa/70w
xL8IluRugoZW8pcNAhqQbnm1TLShzZ9antsCD+fys4EO7M7SlTg=
=Achb
-----END PGP SIGNATURE-----
--=-=-=--

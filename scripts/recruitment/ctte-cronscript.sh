#!/usr/bin/env bash
set -euo pipefail

# Copyright 2021 by Philip Hands under the terms of the GNU GPL, version 2 or later

# This script is intended to be run by cron on a regular basis, to send a batch
# of nomination encouragements to randomly selected DDs, while only doing that
# once per person. It defaults to sending 10, which can be overridden in
# the file pointed at by CONFIG_URL below.

# maximum limit for the number of mails to be sent:
MAX_MAILS=50
# the URL where we fetch the file containing the MAILS_TO_SEND=... line
CONFIG_URL=https://salsa.debian.org/debian/tech-ctte/-/raw/master/scripts/recruitment/README.txt

# abort:
# bails out with an error message
abort() {
    echo "$0: ERROR: $* (\$?=$?)" >&2
    exit 1
}

# number_to_send:
# grabs a copy of a configuration file, uses that to determine the number of
# mails to send per invocation, and echos that number.
number_to_send() {
    MAILS_TO_SEND=$(curl --cert-status -s $CONFIG_URL |
                        sed -un '1,/^MAILS_TO_SEND=/s/^MAILS_TO_SEND=\([[:digit:]]\+\)\s*$/\1/p')

    if [ -z "$MAILS_TO_SEND" ] ; then
        abort "\$MAILS_TO_SEND not set in $CONFIG_URL" >&2
    fi

    if ! expr "$MAILS_TO_SEND" : [0-9][0-9]*$ >/dev/null ; then
        abort "\$MAILS_TO_SEND='$MAILS_TO_SEND' (not a number)"
    fi

    if [ "$MAILS_TO_SEND" -gt "$MAX_MAILS" ] ; then
        abort "too many mails specified ($MAILS_TO_SEND > $MAX_MAILS)"
    fi

    if [ "$MAILS_TO_SEND" -lt 0 ] ; then
        abort "too few mails specified ($MAILS_TO_SEND < 0)"
    fi

    echo "$MAILS_TO_SEND"
}

# single_mailshot:
# A support function that takes an email address,
# substitues that into the email template,
# and sends the result.
single_mailshot() {
    sed -e 's/%VICTIM%/'"$1"'/;s/%DATE%/'"$(date -R)"'/' ~/local/ctte-nominations.txt |
        /usr/sbin/sendmail -t
}

# ctte_nominators:
# takes an optional parameter, how many nominations to generate (default: 1)
# The list of all accounts in the Debian group from LDAP, is filtered using
# the list of people already mailed (~/ctte-dontspam), and the resulting list of
# potential victims is then shuffled to generate the number of nominators
# required.
#
# The randomisation is done based on a password that is the concarnation of
# today's Testing Release.gpg, and todays date. The idea here is that it is
# possible to independantly reproduce the ordering in order to confirm that the
# person doing the mailing is not introducing some sort of bias.
#
# Of course, if you wanted to game this somehow, you could look at the output of
# the script, and decide not to send it when it was somehow unsatisfactory, so
# if that ever becomes an issue I'd recommend deciding on a regular time to run
# the script, and automate it, to avoid that.
ctte_nominators() {
    ldapsearch -x '(&(keyfingerprint=*)(supplementaryGid=Debian)(!(onVacation=*)))' uid |
        sed -ne 's/uid: //p' |
        fgrep -v -f ~/local/ctte-dontspam |
        shuf -n${1:-1} --random-source=<(openssl enc -aes-256-ctr -pass pass:"$(curl -s http://ftp.debian.org/debian/dists/testing/Release.gpg; date --rfc-3339=d)" -nosalt </dev/zero 2>/dev/null )
}

# ctte_mailshot:
# Takes an optional number, calls ctte_nominators to get today's list, and
# then mails each of them in turn, and adds their name to the dontspam list.
ctte_mailshot() {
    echo "# mailshot for $(date --rfc-3339=d)" >> ~/local/ctte-dontspam
    for uid in $(ctte_nominators $1) ; do
        single_mailshot $uid &&
            echo "$uid" >> ~/local/ctte-dontspam
    done
}

# main program starts here:

VICTIMS=$(number_to_send)

if [ "$VICTIMS" -gt 0 ] ; then
    ctte_mailshot $VICTIMS
fi

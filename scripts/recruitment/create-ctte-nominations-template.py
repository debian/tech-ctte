#!/usr/bin/python3
# Copyright 2024 Timo Röhling <roehling@debian.org>
# SPDX-License-Identifier: MIT
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.
#
from gnupg import GPG
from email.charset import Charset, QP
from email.message import Message
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication
from email.encoders import encode_noop
from email import policy

cs = Charset("utf-8")
cs.header_encoding = QP
cs.body_encoding = QP
text = MIMEText(
    """\
Hello,

As part of the push to get more nominations for the Technical Committee,
I'm sending out a few invitations to nominate people, and this is one.

You don't need an invitation to nominate people, but I'm hoping that by
doing this I'll prompt you to consider the people you know well in the
project, and so encourage you to suggest good candidates that might not
have already been thought of.

For instance, if you have personal experience of someone as being a wise
head and/or who has a track record of calming fraught situations (even
if they might be mostly unknown outside whatever niche of the project
that you both have in common) please take this as a request to nominate
them.

Nominations should be sent to:  debian-ctte-private@debian.org

Just send their name if that's all you have time for, although something
describing why you thought of them would be nice too, and if you have
time to dig out a reference or two to illustrate, that would be great.

We ask them if they're willing, so don't worry about forcing anyone to
do anything.  Also, we won't publish your name, or tell the nominee who
nominated them.  People are generally very pleased to have been
suggested, even if they decide to decline the nomination.

I hope you don't mind the intrusion, but if you do please tell me that,
so that I can either improve or abandon this experiment as appropriate.


Cheers
Timo


P.S. If you're wondering:  "Why is Timo asking me? He knows I ..."
Well, TBH, I didn't.  You were chosen by something that boils down to:

  list_all_developers_not_yet_asked | shuf -n 10

If this has resulted in you getting a mail that strikes you as
pointless, please accept my apologies -- it won't happen again.

Script, and text of this e-mail, due to Phil Hands; lightly edited by Sean
Whitton and Timo Röhling.
""",
    "plain",
    _charset=cs,
)

gpg = GPG(options=["-u1403F4CA"])
basetext = text.as_string().replace("\n", "\r\n")
signature_payload = str(gpg.sign(basetext, detach=True))
assert signature_payload
signature = MIMEApplication(
    signature_payload, 'pgp-signature; name="signature.asc"', encode_noop
)

msg = MIMEMultipart(
    "signed",
    protocol="application/pgp-signature",
    micalg="pgp-sha512",
    policy=policy.SMTP + policy.strict,
)
msg["Subject"] = "Encouraging nominations to the Technical Committee"
msg["From"] = "Timo Röhling <roehling@debian.org>"
msg["To"] = "%VICTIM%@debian.org"
msg["Date"] = "%DATE%"
msg.attach(text)
msg.attach(signature)

print(msg.as_string(unixfrom=False))


Intro Sheet for new Technical Committee Members
===============================================

# References

## Constitution §6

[§6](https://www.debian.org/devel/constitution#item-6) from the constitution is
our bible for how we're supposed to work publicly

## Standard Resolution Procedure (§6.3.1)

We use the standard resolution procedure
[§6.3.1](https://www.debian.org/devel/constitution#item-6) using Don's
pocket-devotee script, which allows us to get condorcet voting in shell. Please
use the `><=` format for expressing your votes.

e.g.
```
A > B = C > D = E = F
```

# Tools

## Public mailing list

Make sure you are subscribed to
[debian-ctte@lists.debian.org](mailto:debian-ctte@lists.debian.org), the public
mailing list where all public matters are discussed.

## Private alias

We have a private alias which you'll get subscribed to:
[debian-ctte-private@debian.org](mailto:debian-ctte-private@debian.org) which we
should be making use of when needed. By default we should work publically, but
for all sensitive things, keep the private list in explicit CC. It's good for
the parties we're privately talking to for them to know that the rest of the TC
is in the loop, as well for our (personal) archival.

## tech-ctte pseudo-bug

https://bugs.debian.org/tech-ctte

Beware of list-vs-bug mails; we try to have all issues separated in bug
reports, which get to the list; so for non-meta discussions, we should always
only write to the bug.

## Regular IRC meetings

The TC holds regular IRC meetings in #debian-ctte, logged using
http://meetbot.debian.net, on an approximate monthly schedule.

## Git repository

The committee's git repository is at:

https://salsa.debian.org/debian/tech-ctte

## Shared calendar

The git repository contains a
[meetings.ics](https://salsa.debian.org/debian/tech-ctte/raw/master/meetings.ics)
calendar file to list the future meetings, both IRC and IRL.

# Principles

## Transparency to each other

We want to provide more transparency to each other in our processes, don't feel
that a "I agree to your long mail" mail would be superfluous, it's definitely
not.


# Act sane

For the rest, apply your very sane judgment, and please state your
disagreements when you have them!

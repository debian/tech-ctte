
The technical committee has served the project for many years, acting as a
conflict resolution body of last resort.  The TC is established in the Debian
Constitution (https://www.debian.org/devel/Constitution#item-6), which means
that it's a highly regulated body and it's hard to change it. The current setup
of the TC has several problems that need to be addressed.

This document is split between first a description of the current situation and
the problems identified, followed by a bunch of different proposals that we
could implement to try to solve said problems.

Problems
========

Perception
----------

Because everything is mandated to be public, discussions must take place in a
public forum where anybody can jump in and add wood to the fire. This can be
very taxing, so there are community members that just check out and refuse to
participate in the discussion even when their input would be valuable. And even
those who participate can feel that their voice is being drowned by whoever
sends the most emails. The committee strives to hear all opinions equally
regardless of how many emails were sent, but the participants of the discussion
can still end up with a bitter taste because of how it devolved.

Going to the TC is seen as a nuclear option, as a stick to beat others with.
This is partly because of how the Constitution establishes that the role of the
committee if to be a last resort decision maker. But also because of historical
reasons that are very hard to change, even when the committee members have all
changed by now.

People don't like being on the "losing" side of a decision, but even if they
are on the "winning" side of it, lots of people don't want the level of
flamewar and attention that an issue raised to the TC brings. Some people would
rather orphan a package than participate in a discussion with the TC.

Social vs Technical
-------------------

Most of the problems that reach the TC are not purely technical. There's a
lot of "human stuff" going on. People with different goals or interests that
fail to agree on how to move forward, but the conflict is not at the technical
side. For discussions around social issues, discussing in the open is like
being on public trial. It's very stressful and very hard to find a good
resolution.

On top of this, because this is the *Technical* committee, we're forced to
focus on the technical side of a problem. In various occasions this means that
there's really no solution we can offer. We're not explicit mediators, we don't
have the authority to be mediators and we're definitely not set up in a way
that leads to good mediation outcomes.

Speed
-----

In many occasions in the past, the committee took too long to make a decision.
There's many reasons for this. Perhaps we wanted to be thorough in looking at
the problem from all angles. Perhaps we wanted to explore different
alternatives to solve the issue. Perhaps we were busy with other priorities and
the discussion just moved slowly. Whatever the case, the long time to
resolution also devalues the role that the committee has in solving issues
(technical or not technical).

If the TC is going to be of value to the project it needs to act quickly when
dealing with urgent matters (for example, right before a freeze).

It's important to note that the questions brought to the TC typically have no
easy and quick solution, that's why they were brought to the TC in the first
place. Still, we could do a lot better.

No design work
--------------

One of the constraints that the TC has to deal with is that we can't do any
design work, it can only choose between options already presented to it. A
consequence here is that even when we have a group of experienced individuals
from different areas of Debian that can maybe come up with great ideas to solve
a problem, we can't really put those ideas to work, as we can't do design work.
Several times it has happened that a discussion that was interesting in nature
had to be stopped because we were falling into the trap of doing design work.

This has the added consequence that the TC is seen (by some) as not providing
any value to the project, because we aren't creating anything, "we just say
no".

Also, related to this is the fact that the TC has the power to make technical
decisions that developers then need to implement, without being involved in
either the design or the implementation of those decisions. This can be seen by
some developers as too much power with too little responsibility.

No-decision decision
--------------------

A lot of the issues that come to the TC end up being closed with us declining
to rule. There can be many reasons for this:

* We were asked to override a delegate (which we can't do)
* We were asked to do design work (which we also can't do)
* We were asked for advice and gave some advice (no ruling is required for
  giving advice).
* The issue got resolved on its own before the TC reached a decision (see the
  "Speed" section above).

This might be ok and be part of the nature of the TC. Some committee members
hold that this is fine and that it's not a problem. Still, it's something to
take into account when thinking about the current situation.

Proposals
=========

There's a bunch of things that we can do. Some may imply changing the
Constitution, some may not. We should consider how to maximise the
value that the TC brings to the project, not how hard the proposal would be to
implement (i.e. it's ok if we need to change the Constitution). Some of these
proposals complement each other, while others contradict each other. They are
here to spark thought, not as a thought out plan (yet). We should adopt the
subset that makes the most sense.

Private Discussions
-------------------

One way to solve the perception issue is to have a way for people to have
private discussions with the TC.

There's a range of options between being fully private (moving the TC mailing
list to private, having a private IRC channel and only informing people of
decisions in public when they are final) and fully public (basically the
current state - while we do have a private mailing list, it's very rarely used,
practically everything happens in the open).

The fully private extreme would hurt the committee's credibility and probably
lead to a lot of miscommunication.  But it should be possible to find a middle
point in this range that still keeps important discussions in the open while
allowing social issues to be solved privately. 

It's common for DDs to communicate between them in private and there's no
reason why people couldn't approach the TC privately, as long as the important
decisions and discussions are still done publicly.

**Proposal 1**: let developers raise issues privately to the TC when they feel
that they need advice or help in dealing with a hairy issue. Issues that need a
public vote would still need to be raised and discussed in public, but issues
that would lead to a "no-decision" decision, can reach a more friendly
resolution this way. This proposal implies having a documented way of raising
an issue privately and a set of expectations of what happens when this action
is taken.

As long as no decisions are made in private, this wouldn't require a change to
the Constitution. However, it might be good to amend section 6.3 to make
explicit which discussions can happen privately and which ones need to happen
publicly.

Allow the TC to be invoked early
--------------------------------

The requirement to make decisions as a measure of last resort means that by the
time the committee is called to action, most issues have already become a
flamewar where no matter the result people will end up unhappy. Removing this
requirement would allow the TC to get involved earlier, helping developers find
consensus rather than beating them with a stick.

For this change to be successful, there should be a clear way of invoking the
TC that doesn't imply a "nuclear option". A way of asking for help solving a
technical disagreement without generating resentment.

Developers don't always know how to solve problems before bringing them to the
TC and that's ok. If we can find a way to let the TC help with conflict without
creating "winners" and "losers" we could make it a much more useful body.

**Proposal 2**: Modify the Constitution to allow the TC to get invoked early,
clarifying how that works.

Mediation body
--------------

The project as a whole will always need help solving disagreement that is
social rather than technical in nature. In different ocassions, this role has
been played by the DPL, the TC, the Community team (pka Anti-Harassment team).
None of these are actually explicitly designated as mediation bodies, which
sometimes gets in the way of achieving the desired resolution. So, it might
make sense to designate a body explicitly for that.

As work done inside the Debian is inherently technical, it's hard for an issue
to be *purely non-technical*, there's always something technical behind the
conflict.  But in many conflicts, the *issue that needs solving* is of a social
nature rather than a technical nature.

**Proposal 3**: Explicitly delegate the mediation task for solving social
conflict between developers, when no code-of-conduct violation is in place.
This could be to:

a. A new group of developers
b. The Community Team
c. The Technical Committee.

Regardless of who is delegated for this task, involved processes would need to
be well documented, so that Debian community members know what to expect when
they get involved in a mediation.

If the TC is chosen as the mediation body, this would require a change in the
Constitution. Otherwise, it's just a DPL delegation.

Whether or not the TC is chosen as the mediation body this can help define the
role of the TC in Debian. If chosen, then we need to establish the necessary
processes. If some other body is chosen, then we can re-direct social issues to
this team and help find quicker solutions to problems.

Allow design work
-----------------

As mentioned, the restriction on the TC only being able to choose between
options limits the work that the TC can do.  It also limits the legitimity that
the committee has, because it's seen as a bunch of people that just issue
decisions without doing any of the work.

One possibility would be to allow design work to happen from the TC in the form
of proposals. These proposals wouldn't have the weight of a TC decision that
must be implemented and would instead act as guidance on how a certain problem
might be resolved.  This would be a significant change in the role of the TC,
over time it would mean that the work done as TC members changes more to
"thinking up solutions" than to "just saying no".

**Proposal 4**: Modify the Constitution to allow the TC to do design work in
the form of proposals. These proposals wouldn't override developers or tell
individual maintainers what to do, but rather should guide the project towards
a technical goal.

Split responsibilities into separate groups
-------------------------------------------

Some of the criticism that the TC has received is that it has too many roles,
some of them explicit (make decisions when developers can't agree, overrule a
developer, give advice) and some implicit (mediate social conflict, act as a
group of elders). And so instead of adding more explicit roles (as seen in
proposals 2 and 3 above), it would be better to split these roles into separate
bodies, created from scratch:

* Advisory body (give advice § 6.1.5, make technical proposals [Proposal 3])
* Mediation body (solve social conflict [Proposal 2])
* Technical decision resolution body (decide when developers can't agree § 6.1.2, override developers, § 6.1.4)

**Proposal 5**: Abolish the TC and split it into separate roles. This would of
course require changing the Constitution and there are a bunch of open
questions regarding who gets to do what and how the members of each body should
be elected.

Please note that the TC members don't actually want this, but this proposal has
been included here for completeness as it has been suggested by other
developers during discussions. There's a lot of unknowns of what exactly this
proposal would look like and in order for it to get adopted someone would need
to clearly outline each of the roles, resposibilities and processes.

People that want this proposal to actually happen would need to volunteer time
and energy to make it a reality.

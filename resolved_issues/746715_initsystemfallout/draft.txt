    The issue of init system support recently came to the Technical
    Committee's attention again.[1]

    For the record, the TC expects maintainers to continue to support
    the multiple available init systems in Debian.  That includes
    merging reasonable contributions, and not reverting existing
    support without a compelling reason.

    [1] See #746715 for background.

* Issue
** Lack of communication from python maintainer
** Delay in uploads of new versions to unstable/experimental
** Coordination with packaging helpers
* Possible Solutions
** Change maintainers
*** Team of volunteers formed by Sandro Tosi
*** Team of volunteers formed by Jakub Wilk
** Decline to change maintainers; suggest increased communication
** Further Discussion
* Open Questions

To: debian-devel-announce@lists.debian.org
Subject: Call for (self-)nominations for a Technical Committee seat

Dear Debian Contributors,

Our Constitution imposes an expiry on Technical Committee terms, and it's that 
time of the year again: Keith Packard <keithp>'s term will expire on December 
31st 2017. The Technical Committee would like to thank Keith for serving since 
November 2013, in addition to all the other work he does for Debian.

To fill this seat, we are soliciting nominations, including self-nominations. 
To nominate yourself or someone else, please send an e-mail to
debian-ctte-private@debian.org with the subject "TC Nomination of loginname", 
where loginname is the nominee's Debian account login¹. Please let us know in 
the body of the e-mail why the nominee would be a good fit for the TC, 
specifically instances where the nominee was able to help resolve 
disagreements, both technical and non-technical. We would like to start our 
selection process on or about the first of December.

Being a member of the TC does require a time commitment. Members should be 
able to follow e-mail discussions on an ongoing basis and respond within a 
couple of days so that discussions progress. Members should ideally be able to 
spend 10 hours a month for relatively busy months, but typical time 
requirements will be less.

We are in the process of documenting how the TC implements the nomination 
process as outlined in §6.2, including clarifying what is made public (such as 
nominations) and when. Any nominee is of course free to drop off the process 
at any point in time.

Regards,
        OdyX, for the TC

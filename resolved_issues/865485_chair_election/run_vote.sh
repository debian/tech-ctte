../scripts/pocket-devotee \
        --option 'A: Keith Packard <keithp>' \
        --option 'B: Didier Raboud <odyx>' \
        --option 'C: Tollef Fog Heen <tfheen>' \
        --option 'D: Sam Hartman <hartmans>' \
        --option 'E: Phil Hands <philh>' \
        --option 'F: Margarita Manterola <marga>' \
        --option 'G: David Bremner <bremner>' \
        --option 'H: Niko Tyni <ntyni>' <<EOF
keithp: B > A = C = D = E = F = G > H
odyx: B > E > D = C > F = G = H > A
philh: B > F > A = C = D = E = G > H
ntyni: B > A = C = D = E = F = G = H
EOF


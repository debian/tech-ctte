../scripts/pocket-devotee \
        --option 'A: Didier Raboud <odyx>' \
        --option 'B: Tollef Fog Heen <tfheen>' \
        --option 'C: Phil Hands <philh>' \
        --option 'D: Margarita Manterola <marga>' \
        --option 'E: David Bremner <bremner>' \
        --option 'F: Niko Tyni <ntyni>' \
        --option 'G: Gunnar Wolf <gwolf>' <<EOF
ntyni: A > B = C = D = E > F > G
gwolf: A > B = C = D = E = F > G
bremner: A = B > C = D = E = F > G
philh: A = D > B = C = E = F > G
odyx: A = B = C = D = E = F = G
EOF

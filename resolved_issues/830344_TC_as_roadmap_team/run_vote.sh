../../scripts/pocket-devotee \
        --option '1: The TC volunteers to be the Roadmap team' \
        --option '2: The TC volunteers to be part of the regular workflow of the Roadmap team, as an advisory body' \
        --option "3: The TC shouldn't be part of the regular workflow of the Roadmap team. We will always be available for escalations, as usual" \
        --option '4: Further Discussion' << EOF
marga: 3 > 2 > 1 > 4
fil: 3 > 4 > 1 = 2
don: 3 > 4 > 1 = 2
hartmans: 2 > 1 = 4 > 3
odyx: 3 > 2 > 1 > 4
tfheen: 3 > 2 = 4 > 1
EOF


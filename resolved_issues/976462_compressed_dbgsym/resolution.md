# #976462: tech-ctte: Should dbgsym files be compressed via objcopy --compress-debug-section or not?

With the current information available, our decision is that debhelper
should continue to use --compress-debug-section.

This is not a requirement and is not intended to overrule a maintainer;
we have no objection to the debhelper maintainer changing this policy
if different information becomes available in future.

## In favour of compressing detached debug symbols on disk

Compressing the contents is considerably better for their Installed-Size,
enough that it can make the difference between feasible and not feasible
to keep debug symbols for packages of interest installed - particularly
for large C++ packages like web browsers and the KDE suite.

(Median Installed-Size without --compress-debug-section is around 250%
of median Installed-Size with --compress-debug-section, according to
analysis by Jakub Wilk and Elana Hashman. For large packages, not
compressing the detached debug symbols on disk can mean +5.7G of
Installed-Size.)

## Against compressing debug symbols on disk

- The .deb size (and therefore mirror size and download bandwidth
  consumption) is somewhat worse when compressed.
  (Median .deb size without --compress-debug-section is around 75%
  of median .deb size with --compress-debug-section, according to
  analysis by Jakub Wilk and Elana Hashman)

- Some toolchain and debugging tools have had bugs in the past where they
  could not deal with compressed debug symbols, e.g. in valgrind; but these
  bugs were treated as bugs upstream, and fixed.
  (https://bugs.kde.org/show_bug.cgi?id=396656,
  https://bugs.kde.org/show_bug.cgi?id=427969)

- Some debugging tools might still have similar bugs, but nobody was able
  to cite one specifically.

- Some toolchain components cannot deal with compressed debug symbols and
  this puts constraints on the order of operations in debhelper: in
  particular, dwz is not able to act on compressed debug symbols, so dh_dwz
  must run before dh_strip compresses the debug symbols. However, since
  the debhelper maintainer seems happy with the current situation, it seems
  this is not a practical problem.

- debugedit (used in Fedora to process debug symbols, for example as
  Fedora's alternative to our use of -fdebug-prefix-map) also cannot deal
  with compressed debug symbols. However, debugedit is not currently used
  in Debian builds. If we incorporate it into Debian builds in future, we
  can either run it before dh_strip like we do for dwz, or reconsider the
  use of objcopy --compress-debug-section at that time.

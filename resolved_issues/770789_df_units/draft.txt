In 770789, the Technical Committee was asked to override the decision
of upstream and the maintainer of df to not include i in the units
output when asked for IEC output (2^10).

The CTTE declines to override the decision of the maintainer and
upstream.

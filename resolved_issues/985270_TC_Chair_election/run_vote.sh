#!/bin/sh
../scripts/pocket-devotee \
        --no-default_option \
        --option 'A: Margarita Manterola' \
        --option 'B: David Bremner' \
        --option 'C: Niko Tyni' \
        --option 'D: Gunnar Wolf' \
        --option 'E: Simon McVittie' \
        --option 'F: Sean Whitton' \
        --option 'G: Elana Hashman' \
        --option 'H: Christoph Berg'  << EOF
marga: B > C > F = G > D > E > A > H
bremner: A = G > E = F = C > B = D > H
spwhitton: A = B = C = E = F = G > D > H
gwolf: A = B = C = E = F = G > D > H
ehashman: B > F > C = D = E > A = G > H
ntyni: F > A = G > E > B = D > C > H
myon: A = F > B = C = D = E = G > H
smcv: F > A = B = C = D = G > E > H
EOF

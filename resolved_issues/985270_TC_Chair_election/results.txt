Starting results calculation at Wed Mar 17 16:49:22 2021

/--ABCDEFGH
V: 61245337 marga
V: 13232214 bremner
V: 11121113 spwhitton
V: 11121113 gwolf
V: 41333245 ehashman
V: 24543126 ntyni
V: 12222123 myon
V: 22223124 smcv
Option A "Margarita Manterola"
Option B "David Bremner"
Option C "Niko Tyni"
Option D "Gunnar Wolf"
Option E "Simon McVittie"
Option F "Sean Whitton"
Option G "Elana Hashman"
Option H "Christoph Berg"

In the following table, tally[row x][col y] represents the votes that
option x received over option y.

                  Option
              A     B     C     D     E     F     G     H 
            ===   ===   ===   ===   ===   ===   ===   === 
Option A            3     3     5     4     1     1     8 
Option B      2           3     4     3     2     2     8 
Option C      2     1           4     2     1     2     8 
Option D      2     0     1           2     0     1     8 
Option E      2     2     1     4           0     1     8 
Option F      4     4     4     8     5           4     8 
Option G      1     2     2     5     4     1           8 
Option H      0     0     0     0     0     0     0       



Looking at row 2, column 1, B
received 2 votes over A

Looking at row 1, column 2, A
received 3 votes over B.



  Option A defeats Option B by (   3 -    2) =    1 votes.
  Option A defeats Option C by (   3 -    2) =    1 votes.
  Option A defeats Option D by (   5 -    2) =    3 votes.
  Option A defeats Option E by (   4 -    2) =    2 votes.
  Option F defeats Option A by (   4 -    1) =    3 votes.
  Option A defeats Option H by (   8 -    0) =    8 votes.
  Option B defeats Option C by (   3 -    1) =    2 votes.
  Option B defeats Option D by (   4 -    0) =    4 votes.
  Option B defeats Option E by (   3 -    2) =    1 votes.
  Option F defeats Option B by (   4 -    2) =    2 votes.
  Option B defeats Option H by (   8 -    0) =    8 votes.
  Option C defeats Option D by (   4 -    1) =    3 votes.
  Option C defeats Option E by (   2 -    1) =    1 votes.
  Option F defeats Option C by (   4 -    1) =    3 votes.
  Option C defeats Option H by (   8 -    0) =    8 votes.
  Option E defeats Option D by (   4 -    2) =    2 votes.
  Option F defeats Option D by (   8 -    0) =    8 votes.
  Option G defeats Option D by (   5 -    1) =    4 votes.
  Option D defeats Option H by (   8 -    0) =    8 votes.
  Option F defeats Option E by (   5 -    0) =    5 votes.
  Option G defeats Option E by (   4 -    1) =    3 votes.
  Option E defeats Option H by (   8 -    0) =    8 votes.
  Option F defeats Option G by (   4 -    1) =    3 votes.
  Option F defeats Option H by (   8 -    0) =    8 votes.
  Option G defeats Option H by (   8 -    0) =    8 votes.


The Schwartz Set contains:
	 Option F "Sean Whitton"



-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

The winners are:
	 Option F "Sean Whitton"

-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=


#!/bin/sh
../scripts/pocket-devotee \
        --no-default_option \
        --option 'A: Philip Hands' \
        --option 'B: Margarita Manterola' \
        --option 'C: David Bremner' \
        --option 'D: Niko Tyni' \
        --option 'E: Gunnar Wolf' \
        --option 'F: Simon McVittie' \
        --option 'G: Sean Whitton' \
        --option 'H: Elana Hashman'  << EOF
spwhitton: B > C > A = D = E = F > G = H
bremner: B > A = C = D = E = F > G = H
gwolf: B > A = C = D = E = F > G = H
marga: C > D > A > E > F > B > G > H
ehashman: B = C > A = D = E = F > G = H
ntyni: B > C > A = D = E = F > G = H
philh: B > C > A = D = E = F > G = H
smcv: A = B = C = D = E > F > G = H
EOF

  Whereas:

  1. Our technical objectives are:

    (i) Users who do not do anything special should get
	network-manager along with gnome (in this case, along with
	gnome-core).  These users should continue to have
	network-manager installed, across upgrades.

    (ii) Users should be able to conveniently install and upgrade
	gnome without network-manager.

    (iii) Users who deliberately removed network-manager in squeeze
	(which they will generally have done by deliberately violating
	the Recommends from the gnome metapackage) should not have to
	do anything special to avoid it coming back in wheezy.

    (iv) Users who do make a decision that they do not want to use
	network-manager should not have to read specific
	documentation, or temporarily have network-manager installed,
	risk being exposed to bugs in network-manager's configuration
	arrangements, and so on.

  2. Our technical objectives do NOT include:

    (i) The `gnome-core' metapackage should in some sense perfectly or
        exactly correspond to GNOME upstream's definition of `the GNOME
        Core', specifically including every such component as a hard
        Depends.

    (ii) The contents of any metapackage should be the correct
        expression of the subjective opinion of the metapackage's
        maintainer.

    (iii) Users who choose to globally disable Recommends should still
        get the desired behaviours as described above in point 1.

  3. The solution recommended by the gnome-core maintainers is
     that users who do not wish to use network-manager should have it
     installed but disable it.

     Installing network-manager in these circumstances does
     not fully meet any of the above objectives apart from 1(i).

  5. The alternative solution rejected by the gnome-core maintainers
     is downgrade the dependency to Recommends.

     This solution meets all of the objectives from point 1, except
     that infelicities in teh package manager may mean that the user
     in 1(iii) may need to take action to prevent network-manager
     being reinstalled during an upgrade.

  Therefore:

  6. The Technical Committee overrules the decision of the gnome-core
     metapackage maintainer.  The dependency from gnome-core to
     network-manager-gnome should be downgraded to Recommends.

  7. The Technical Committee requests that the Release Managers
     unblock the update to implement this decision, so that this
     change may be released in wheezy.

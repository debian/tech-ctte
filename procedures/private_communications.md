Private communication
=====================

Section 6.3.3 of the Debian Constitution v1.8 says

> Discussion ... and votes by members of the committee, are made public on the
> Technical Committee public discussion list.

However, as many issues the Technical Committee has to deal with have
non-technical dimensions, sometimes individual committee members or the
committee as a whole engage in private communication, primarily regarding
those non-technical dimensions, both with individuals, and with other teams.

The purpose of this document is to set some expectations and limitations on
our use of private communication channels, in light of the constitution's
emphasis on TC publicity.

Limitations
-----------

The rationale for TC decisions must be stated publicly.  Thus, anything said
or argued privately must be subsequently published if it is to support a
decision.  We will not make any decisions privately: instead, we will use
private communication to try to determine what our options are when we think
that doing so in private will keep temperatures down.

In addition, we might, in response to a private inquiry, say that we think a
so-called "no decision" outcome is likely.  This means people can write to us
in private to ask whether we think filing a bug is appropriate at all, which
can help avoid pointlessly raising temperatures.

Raising an issue privately
--------------------------

You are welcome to get in touch with us privately, at
`debian-ctte-private@debian.org`, for the following sort of things:

- advice on how to handle potentially controversial technical matters

- assistance with de-escalating a technical conflict

- help with the public presentation of a technical argument.

We particular appreciate receiving early notification of a developing conflict
that might eventually result in a request being made for us to decide.

Anything that we receive privately remains private until and unless all
participants give consent for it to be posted publicly.

Moderation of tech-ctte bug reports
===================================

As issues are referred to the Technical Committee, their state of disagreement
and prior discussion varies widely. Some have extensive history covering up to
ten years. Often when issues are referred, the referring side dominates the
discussion. Keeping discussions constructive and receiving the requested
information is difficult at times.

Moderation role
---------------

For each `tech-ctte` bug report there should be a designated moderator. The
role will work in the following way.

 * A Technical Committee member can designate themselves as moderator. The
   choice should be announced to the bug report and recorded in the BTS by
   setting the owner. An assigned moderator role may change consensually at
   a later time.
 * The moderator should inform the submitter about the decision process and
   request initial feedback regarding missing information if any.
 * The role should be assigned quickly after a bug report has been filed.
 * Ideally the moderator should not be directly affected by or involved with
   the matter being discussed.
 * If the discussion volume grows, a moderator should summarize the state of
   discussion unless such summaries exist already and seek consensus when
   reasonable or note items of disagreement. A moderator may also request
   summaries from participants.
 * The moderator should monitor the discussion as a whole. When they see fit,
   they should explicitly request discussion participants to change their way
   of interacting. For instance, a moderator may decide that the views of a
   participant have been heard and request to pause a participant.
 * All relevant parties should be aware that the Technical Committee has been
   asked to decide on a matter. A moderator should inform them when in any doubt,
   and explicitly solicit their input where relevant.

Why moderate?
-------------

The Technical Committee has acquired a reputation of being slow to arrive at
decisions.  The reasons for this are multiple, but waiting for one another is a
common theme. In managing the discussion, it becomes easier for busy Technical
Committee members to catch up, but it also means involving relevant parties
sooner rather than later. Establishing consensus and disagreement items should
help focus the discussion and yield faster and more efficient resolution for
the benefit of all involved.

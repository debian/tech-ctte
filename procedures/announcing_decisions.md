Announcing decisions reached by the Technical Committee
-------------------------------------------------------

Whenever the Technical Committee reaches a decision, it is important it
is properly communicated to all interested project members.

However, we acknowledge that not all decisions are of the same scope,
and sending very niche decisions to a widely read announcement list
such as `debian-devel-announce@lists.debian.org` might be less than
ideal.  On the other hand, announcing decisions only to a high-volume
list such as `debian-devel@lists.debian.org`, particularly during busy
discussion periods, risks drowning an important decision in the
noise. So, we have decided that:

- Decisions will be sent either to
  `debian-devel-announce@lists.debian.org` or to
  `debian-devel@lists.debian.org`, with a tendency towards the first.
- General and wide-ranging decisions will always be sent to
  `debian-devel-announce@lists.debian.org`.
- Very specific, niche decisions will be sent only to
  `debian-devel@lists.debian.org`.
- The decision of which list to post to will be made by the TC Chair.
  <!-- helmut suggests: the decision will be made by the TC member -->
  <!-- calling for the vote -->

An exception to this policy is when deciding on appointing new members to the
TC. The TC vote is conducted in public, on the `debian-ctte@lists.debian.org`
mailing list, but the result is in most cases a recommendation to the DPL. It
is up to the DPL to nominate (or, if necessary and in those cases the
constitution permits, veto) the new TC member.

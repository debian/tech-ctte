Early invocation
================

The problem we face
-------------------

-   It is often not clear to developers how and when to bring issues to
    the TC
-   We must find a way to get the TC to mediate in conflicts without
    creating winners and losers
-   When issues reach the TC, we probably are already past the
    flamewar stage
-   There should be a way to ask the TC to intervene without it being a
    nuclear option -- asking for help solving a technical disagreement
    without generating resentment

Constitutional Context
----------------------

There are two main relevant parts of the Debian constitution. Section
6.3.6 constrains the committee to make decisions as a *last resort*

> The Technical Committee does not make a technical decision until efforts
> to resolve it via consensus have been tried and failed, unless it has
> been asked to make a decision by the person or body who would normally
> be responsible for it.

On the other hand, Section 6.1.5 empowers the committee to offer
advice, and specifically mentions committee members offering informal
advice.

> The Technical Committee may make formal announcements about its
> views on any matter. *Individual members may of course make informal
> statements about their views and about the likely views of the
> committee*.[^1]

[^1]: Emphasis present in the constitution's text

Facilitators
------------

Any developer or group of developers can request that the TC assign a
*facilitator* to work with them in the early stages of a technical
disagreement.

Facilitators are regular members of the TC that can provide advice. Although
this advice is not formally a statement of the TC, we can expect the TC to
defer to some degree to the views of one of their members that has been deeply
involved with the issue.

Facilitators are also DDs, so can carry out all of the normal activities of a
DD, including things like detailed design that are not possible for the
TC. Obviously the facilitator must be clear about when they are acting as TC
facilitator, and when they are acting as regular DDs. Some facilitators may
prefer to avoid any activities that would not be permissible for the TC as a
body.

As with any TC related activity, assigned facilitators may find themselves in
conflict of interest in a given disagreement. We trust members of the TC to
recognize such problems and, if it becomes necessary, recuse themselves from
their role as a facilitator, or even recuse themselves from a later TC vote on
the disagreement.

TC early involvement outcomes
-----------------------------

-   The TC cannot dive in and rule quickly on an ongoing discussion.
-   But if a matter is brought up in a meeting and there is a clear
    consensus as to what the TC's opinion is, it can be an important
    data point for the people involved in the discussion.

TC and timeliness
-----------------

-   The TC's process takes a long time. Relatively simple discussions often
    take months
    -   Even though we could have swifter communication, we tend to
        wait for our monthly meetings to discuss and reach agreements
    -   The idea that the TC can respond in a timely manner to an
        ongoing discussion may well be nonsense
-   But if a formal consensus is not needed, having such discussions
    could be worthwile if highlighted earlier in their lifecycle

Contacting us
-------------

-   Formally, "requesting action from the TC" means "using
    reportbug to contact us"
-   We want to streamline the process and hopefully avoid things
    becoming TC bugs at all sometimes
-   Developers are welcome to contact the committee informally for
    feedback, or to ask for a facilitator.
-   We can talk about your issue, even informally, via IRC
     `#debian-ctte` in OFTC
-   If you prefer, mail a brief summary
    -   To our mailing list (public, archived: `debian-ctte@lists.debian.org`)
    -   To our private alias (`debian-ctte-private@debian.org`)

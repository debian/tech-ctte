Unless they couldn't at all imagine the candidate being accepted by the
committee, any committee member can go ahead and send this non-committal
message as soon as a nomination is received to the committee's private alias.
Other committee members should not regard this as saying anything about that
committee member's opinion about the candidacy beyond the belief that they do
stand some chance.  This procedure is intended to speed up consideration of a
candidate by rapidly resolving the question of whether they would accept a
nomination, without committing existing committee members to very much.

Subject: TC nomination
To: [CANDIDATE]@debian.org
Cc: debian-ctte-private@debian.org

Hi!

As you might know, the Debian Technical Committee is looking for new
members.  We've been soliciting nominations for the positions and your
name has come up.  Would you be interested in being considered?

If so, would it be possible to share with us examples of bugs, and other
discussions, where you feel you've been involved in a conflict in a
constructive way?  That would help our deliberations a great deal.
(We send this exact text, requesting this material, to every candidate.)

Thanks for your work on Debian,

[1]: https://lists.debian.org/debian-devel-announce/2021/09/msg00002.html

- [SIGNATURE]
for the Committee

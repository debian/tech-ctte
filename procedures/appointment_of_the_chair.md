Appointment of the Chair of the Technical Committee
---------------------------------------------------

In general, the procedure in [§6.1.7](https://www.debian.org/devel/constitution.en.html#item-6)
should be followed.

Re-appointment of chair after membership change
===============================================

When new members are appointed to the Technical Committee or within three
months of a member resigning from the Technical Committee, the current
chair should announce their intention to vacate the position within two weeks.

This will trigger a new election to which all members of the Technical Committee
(including the vacating chair if the chair is still a member) are eligible.

When the terms of committee members expire on December 31st, but new
appointments are expected to be made by the end of January, the chair does not
resign until after those appointments are made.  This is to avoid two
elections in quick succession.

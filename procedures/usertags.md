Use of usertags to manage Technical Committee bugs
==================================================

# Introduction

The [tech-ctte bugs page](https://bugs.debian.org/tech-ctte) has the
ability to organise bugs based on their state; this is done by use of
usercategories and usertags. This guide outlines their current use.

The aim of these is to help TC members keep track of the state of each
open bug; obviously this is entirely optional!

# User categories

The default usercategory for the tech-ctte@packages.debian.org user is
`cttestate`, which is set up thus:

```
user tech-ctte@packages.debian.org
usercategory cttestate [hidden]
 * CTTE State
   + Seeking Consensus [0:tag=consensus-seeking]
   + Discussion [1:tag=discussion]
   + Drafting [2:tag=drafting]
   + Voting [3:tag=voting]
   + Published [5:tag=published]
   + Complete [6:pending=done+tag=published]
   + No Decision Decision [7:pending=done]
   + Unknown State [4:]
thanks
```

See [the request server
documentation](https://www.debian.org/Bugs/server-request) on the
syntax of this command. Note that it's a write-only interface, so if
you update it for any reason **also update this document**.

# User tags

To use this, mail the control server, setting user to
tech-ctte@packages.debian.org and setting the appropriate usertag from
the list above, for example:

```
user tech-ctte@packages.debian.org
usertags 123456 = discussion
quit
```

This would cause bug 123456 to appear under "Discussion bugs" on the
BTS page.

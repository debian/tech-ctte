18:59:21 <dondelelcaro> #startmeeting
18:59:21 <MeetBot> Meeting started Wed Nov 25 18:59:21 2015 UTC.  The chair is dondelelcaro. Information about MeetBot at http://wiki.debian.org/MeetBot.
18:59:21 <MeetBot> Useful Commands: #action #agreed #help #info #idea #link #topic.
18:59:24 <Mithrandir> here
18:59:29 <dondelelcaro> #topic Who is here?
18:59:31 <Mithrandir> Tollef Fog Heen
18:59:35 <hartmans> Sam Hartman
18:59:38 <OdyX> Didier Raboud
18:59:39 <keithp> dondelelcaro: bet it looks a lot like the google streetview today :-)
18:59:40 <dondelelcaro> Don Armstrong
18:59:44 <keithp> Keith Packard
19:00:31 <dondelelcaro> aba, bdale, vorlon: ping
19:00:35 <vorlon> Steve Langasek
19:00:39 <vorlon> (sorry, nickserv was sassing me)
19:01:01 <dondelelcaro> #topic Next Meeting?
19:01:11 <dondelelcaro> Currently the winner is      Option I "Wednesday 19:00 UTC (Decemeber 30th)"
19:01:18 <dondelelcaro> (well, it's one of three winners)
19:01:29 <dondelelcaro> if that's not ideal for you (which it might not be) please update your votes
19:01:51 <dondelelcaro> I'll add the Jan meeting as well so we can have that updated
19:01:52 <OdyX> I'm VAC from Dec 21 to Jan 3
19:01:53 <keithp> all the same to me; HPE is closed all week
19:02:06 <dondelelcaro> OdyX: OK, so none of those work at all?
19:02:19 <OdyX> nope. I'll skip the Dec meeting as it seems.
19:02:32 <dondelelcaro> OdyX: OK, that's fine. Would the 16th work potentially?
19:02:33 <bdale> here
19:02:43 <bdale> Bdale Garbee
19:03:06 <OdyX> dondelelcaro: yep
19:03:20 <dondelelcaro> #action dondelelcaro to add the 16th as an option for December and update the meeting poll
19:03:28 <dondelelcaro> #action everyone to update their votes if necessary in the meeting poll
19:03:35 <dondelelcaro> #topic #741573 Menu systems - Debian Policy followup
19:03:37 <hartmans> I could not make the 16tho, but I'm totally fine missing one meeting.
19:03:52 <dondelelcaro> hartmans: cool; we'll vote and try to figure that out, I think
19:03:55 <hartmans> So I will not rank below FD
19:04:39 <dondelelcaro> I've started the discussion of the menu systems bug now rather later than I should have
19:04:44 <dondelelcaro> and with no subject (ugh)
19:05:05 <OdyX> it's good. I had zero capacity this month, so at least it moves
19:05:24 <hartmans> I am going to have basically no capacity until the 18th
19:05:27 <dondelelcaro> if a few people could help drive that discussion, that would be awesome
19:05:28 <vorlon> the next meeting is the last for me and bdale, right?  will there be a going away party? ;)
19:05:38 <dondelelcaro> vorlon: that's true, it will be.
19:05:43 <dondelelcaro> I suppose we should
19:05:46 <OdyX> Champagne!
19:05:47 <keithp> vorlon: if you're home, I'll take you out for lunch
19:05:50 <hartmans> O, you're always we/lcome to throw peanuts from the gallary at future meetings.:-)(
19:05:51 <bdale> vorlon: I'm betting the real party will be the first one we're not at... ;-)
19:06:16 <Mithrandir> haha. :-)
19:06:22 <vorlon> :-)
19:06:28 <Mithrandir> we can have a going-away-party for you at debconf?
19:07:01 <vorlon> ah, not at all certain that I'll make DebConf next year fwiw
19:07:09 <OdyX> 'nyway :)
19:07:13 <vorlon> anyway, sorry to distract
19:07:14 <dondelelcaro> #topic #771070 Coordinate plan and requirements for cross toolchain packages in Debian
19:07:43 <dondelelcaro> Wookie e-mailed me to say that he was still working on a summary, so I'll just let this one sit until that happens
19:08:10 <dondelelcaro> unless someone thinks we should do something differently
19:08:28 <dondelelcaro> #topic #802159 New OpenSSL upstream version
19:08:54 <dondelelcaro> The SRMs are going to try to get some cycles to look at this issue, which is basically all that we could ask for, I think
19:09:24 <dondelelcaro> I think we can just leave it alone and assist if we're asked
19:09:39 <Mithrandir> maybe monitor it a bit and poke folks if it goes quiet?
19:09:42 <Mithrandir> but yeah
19:09:47 <dondelelcaro> Mithrandir: yeah, that sounds good to me
19:10:15 <dondelelcaro> #agreed monitor #802159 and assist if asked
19:10:22 <dondelelcaro> #topic #636783 Constitution: super-majority bug & #795854 Constitutional Amendment: Fix duplicate section numbering (A1)
19:10:53 <dondelelcaro> I believe a vote has been called for this, so we can probably close it once that happens
19:10:56 <OdyX> hartmans called for votes, process is now pending on the secretary to launch the machinery, afaik
19:11:07 <dondelelcaro> yeah
19:11:34 <dondelelcaro> #action dondelelcaro to close once the voting process starts
19:11:41 <dondelelcaro> #topic #797533 New CTTE members
19:11:47 <dondelelcaro> we still need more nominees
19:11:53 * OdyX nods.
19:12:03 <hartmans> I also think we might want to compile the info we have on the ones we've received
19:12:10 <dondelelcaro> yeah, I agree
19:12:19 <dondelelcaro> does someone have the cycles to do that?
19:12:41 <OdyX> some of us should explicitely ask people they would like to see onboard, and CC the -private list
19:12:45 <hartmans> In at least one case the justification was entirely non-technical, so I'd probably want to follow up and ask the candidate to describe their technical contributions too.
19:12:49 <hartmans> what we have is useful though
19:13:02 <dondelelcaro> OdyX: I agree
19:13:41 <dondelelcaro> why don't all of us nominate at least one person we'd like to serve with on the committee?
19:14:27 <OdyX> yes please.
19:14:28 <dondelelcaro> #action everyone to nominate at least one person that they would like to serve with on the comitttee?
19:14:54 <dondelelcaro> #topic #795855 Formal cloture vote
19:15:07 <dondelelcaro> for this, I think that hartmans was going to write a summary, and close the bug
19:15:25 <hartmans> I'm sorry, work has exploded.
19:15:31 <hartmans> I will do so second half of this month.
19:15:35 <hartmans> Adding reminder to my calendar.
19:15:48 <dondelelcaro> hartmans: cool; no real rush, since we're going to close the bug anyway
19:16:12 <dondelelcaro> #topic Additional Business
19:16:15 <OdyX> or we could just close and be done with it.
19:16:27 <dondelelcaro> that too
19:16:45 <dondelelcaro> hartmans: whichever you decide to do is fine by me
19:17:01 <hartmans> I think the summary is interesting.
19:17:10 <hartmans> bdale and I had what I think was a great discussion and I'd like to capture it.
19:17:13 <bdale> I'd just close it, but given the circumstances in which this came up, I suspect I'm the wrong person to voice a deciding opinion
19:17:17 <dondelelcaro> got it
19:17:30 <OdyX> yeah. If you have (or can get) bandwidth
19:17:39 <hartmans> I will schedule it.
19:18:02 <dondelelcaro> anyone have anything else?
19:18:20 <vorlon> nothing here
19:18:31 <dondelelcaro> I think we're closing out most of the open issues right now, and we'll only have a few minor things going forward
19:18:37 <dondelelcaro> plus the nominees
19:18:42 <OdyX> No. The most pressing is getting a long enough list of interested nominees.
19:19:02 <keithp> always hard to find people both willing and able to serve
19:19:03 <bdale> is there a list of the current nominees somewhere?  I can troll back through email if I need to.
19:19:04 <dondelelcaro> yeah
19:19:32 <dondelelcaro> OdyX: could you write up a list of the nominees and send them out to the -private alias?
19:19:35 <OdyX> bdale: nowhere public, of course, but the conversations have been CC'ed to our private list.
19:19:38 <bdale> ok
19:19:43 <OdyX> dondelelcaro: yes. I'll do straight away.
19:19:50 <dondelelcaro> OdyX: awesome, thanks
19:20:02 <bdale> indeed.  thanks
19:20:27 <bdale> it's pretty clear that there's timidity about jumping in here
19:20:34 <hartmans> nothing here
19:20:53 <bdale> that's understandable, but regrettable.  each of us reaching out to folks is a great idea.
19:21:49 <dondelelcaro> yeah
19:22:01 <dondelelcaro> ok; I personally don't have anything else; good to talk to everyone
19:22:11 <bdale> nothing here
19:22:16 <Mithrandir> nor here.
19:22:54 <OdyX> nothing here. Not unhappy that the load has reduced too. I still stand to my hope that we could eventually get our list of issues down to zero.
19:23:13 <keithp> OdyX: in a perfect world, we would never have anything to do :-)
19:23:16 <dondelelcaro> #endmeeting
18:58:33 <marga> #startmeeting
18:58:33 <MeetBot> Meeting started Wed Jan 16 18:58:33 2019 UTC.  The chair is marga. Information about MeetBot at http://wiki.debian.org/MeetBot.
18:58:33 <MeetBot> Useful Commands: #action #agreed #help #info #idea #link #topic.
18:58:39 <marga> #topic Roll Call
18:58:49 <Mithrandir> Tollef Fog Heen
18:58:49 <marga> Margarita Manterola
18:58:57 <bremner> David &
18:59:04 <ntyni> Niko Tyni
18:59:18 <OdyX> Didier Raboud
19:00:20 <marga> gwolf, fil ?
19:00:51 <OdyX> gwolf mentionned he'd be late. No recent news of fil
19:00:58 <gwolf> o/
19:01:02 <gwolf> Just arriving
19:01:05 <gwolf> Gunnar Wolf
19:01:18 <marga> I didn't prepare the agenda and didn't go through AIs... Sorry about this.  I think we should start with debootstrap that is the most pressing issue.
19:01:31 <OdyX> Yep.
19:01:37 <marga> #topic #914897 - should debootstrap disable merged /usr by default?
19:01:52 <OdyX> There are two realistic paths:
19:02:21 <gwolf> I'm a bit rusty, haven't thought about the bugs lately, so... Well, please excuse me if I tend to follow already considered roads :-]
19:02:24 <OdyX> A) confirm debootstrap maintainer's choice and let the world follow, during freeze.
19:03:02 <OdyX> B) override them, but say we want to go usrmerged for buster+1, so invite changes as early as possible.
19:03:55 <OdyX> There are variations of both options of course, but baseline of our discussion is "rusty around the edges in buster" vs "release goal for buster + 1.
19:05:06 <OdyX> A) is not very comfortable as we have a stable with intermediate state: buildds (and build chroots) are not merged where user machines are.
19:05:40 <OdyX> (newly-setup machines that is; as non usr-merged buster hosts will exist)
19:05:42 <Mithrandir> I think we're effectively required to support both merged and unmerged now for buster, or we force through usrmerge for everybody (which is a bad idea, I think)
19:05:49 <Mithrandir> we're past transition freeze
19:06:10 <OdyX> For what I've seen, things work fine.
19:06:25 <Mithrandir> except the things which don't, sure
19:06:47 <OdyX> indeed, short-sighted statement, withdrawn.
19:07:37 <Mithrandir> (dpkg -S confusion, possible incompatibility between packages built on merged systems vs unmerged ones, there's a list)
19:07:48 <Mithrandir> it's not "the-sky-is-falling" critical, but it's unfortunate.
19:07:56 <gwolf> As for the political impedance this brings... While forcing a decision on maintainers is within our defined powers, it creates a... to put it mildly, bad mood and resentment.
19:08:33 <gwolf> As Mithrandir says, there's a list, and there have been observed cases where Problems Happened™
19:09:06 <OdyX> Currently, I feel I stand somwhere along the lines of "usrmerged is desirable, symlinks have weird effects on dpkg semantics and are a shortcut, we know how to address such problems in long transitions, but long transitions can be too long"
19:09:33 <OdyX> I could live with a "shipping files (but exceptions) is to be RC in buster+1"
19:09:47 <Mithrandir> yeah, it has social implications, but it's also not completely clear to me who should make design choices about things like this. It's not clear that it's something that the debootstrap maintainers should just do, it's almost like it should be coming out of the policy process (except that documents practice, it doesn't lead, so not completely appropriate)
19:10:02 <gwolf> Right, because it affects the distribution as a whole
19:11:01 <OdyX> I could live with a "shipping files (but exceptions) outside of /usr is to be RC in buster+1"
19:11:09 <OdyX> (sorry, didn't re-read what I wrote)
19:12:03 <marga> So, is the general sentiment that we should go with B (revert, make it a goal for buster+1), then?
19:13:52 <gwolf> I think it is...
19:14:03 <Mithrandir> possibly, but then under "set technical policy" not "override maintainer".
19:14:14 <gwolf> Even with what I argued - We managed to observe some issues
19:14:18 <Mithrandir> and it's not clear it solves the problem, since we have already-transitioned systems.
19:14:29 <bremner> a pity smcv is not here.
19:14:30 <marga> I'm confused, wouldn't we be overriding the debootstrap's maintainer then?
19:14:50 <gwolf> Mithrandir: I think we could say, "TC will override maintainer now, and set technical policy at Buster release + 1 day"
19:15:10 <OdyX> I don't think we should refrain from overriding if that's our opinion; we should be sure, but do it.
19:15:29 <Mithrandir> I need to think about whether I think it's worth overriding the maintainer for, due to the social costs.
19:16:31 <gwolf> Mithrandir: You said, "there's a list". I am aware only of the two cases of issues you mentioned - Do you remember any others?
19:16:33 <OdyX> It's not crystal clear to me what the position of the d-i (debian-boot) team is. It feels it's mostly Hideki's position, which others "going along"
19:16:43 <gwolf> (:09:22)
19:16:49 <OdyX> s/which/with/
19:17:02 <bremner> I'm also not clear how committed Hideki is to his position.
19:17:14 <bremner> fwiw
19:17:46 <ntyni> the potential incompatibility between builds on usrmerged and non-usrmerged systems seems the big issue to me fwiw
19:18:27 <Mithrandir> can't you also run into problems with file overwrites if something ships /lib/foo and something else ships /usr/lib/foo ?
19:18:32 <bremner> ntyni: agreed. It depends how much we expect "distributing non-chroot builds by users" as something we think is important not to break?
19:18:39 <Mithrandir> (which is generally a bad idea)
19:18:52 <OdyX> https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=914897#312 is the last message of Hideki on the matter AFAI
19:19:05 <marga> Indeed, is the incompatibility regarding the build setup not something that could be solved?  Because even for buster+1, we wouldn't want to have such incompatibility
19:19:23 <gwolf> ntyni: For a large degree, this can be mostly averted by accepting only source-only uploads (and ensuring official buildds don't usrmerge yet)
19:19:38 <ntyni> sure, it's mostly worked around already
19:19:44 <OdyX> Ah no: #337 is later; "yes, However, if it'll be a blocker for release during freeze, it should
19:19:47 <OdyX> be reverted.
19:19:47 <gwolf> ...But I know it's not easy to mandate discarding binaries always...
19:19:47 <bremner> iiuc, the issue is not with official uploads
19:19:49 <OdyX> "
19:19:51 <Mithrandir> gwolf: that only solves the case for us, not for random ISVs distributing packages.
19:19:56 <bremner> that.
19:20:01 <gwolf> Mithrandir: right, right. Good point.
19:20:12 <Mithrandir> so it's one of those where we can mitigate, but not actually fix.
19:20:24 <ntyni> yes, ad-hoc builds is what Ian mentioned in the initial bug report as well
19:20:28 <gwolf> ISVs should follow AM and join Debian </joke>
19:20:35 <gwolf> NM even
19:21:37 <marga> Alright, what do we want to do to move forward with this?  It feels like we should really act quickly
19:22:50 <OdyX> Draft a proposed decision?
19:23:09 <OdyX> I can take time this Friday, ideally not alone.
19:23:44 <Mithrandir> I'm going out for beer on Friday, I don't think you want me drafting after that. :-)
19:23:57 <ntyni> I'm not sure how much the debootstrap default really matters in the whole picture; as Mithrandir said re effectively required to support both merged and unmerged now for buster
19:24:24 <OdyX> I'm off on Friday (CET), so I'd try to do it around 7-8 UTC.
19:25:14 <marga> AM?
19:25:15 <gwolf> ntyni: I can only guess.. But I guess the volume of usrmerged systems is not that large
19:25:21 <OdyX> ntyni: well, testing's debootstrap "just" produces stable hosts with usrmerge ran, right?
19:25:30 <gwolf> popcon ≈ 120 FWIW
19:25:46 <Mithrandir> gwolf: we can't just shrug and claim they're unsupported, IMO.
19:25:51 <OdyX> marga: that'd be AM; but I'm a procrastrinator, so I'd ETA around 14 UTC :-)
19:26:10 <Mithrandir> gwolf: that popcon does not count buster-installed systems, since debootstrap does not use the usrmerge package.
19:26:14 <gwolf> No, of course - They are supported. But we should probably prefer them _not_ to be building packages
19:26:26 <ntyni> what Mithrandir said
19:26:27 <gwolf> Mithrandir: oh :-/
19:26:44 <OdyX> I don't really see how reverting debootstrap's change makes said systems less supported than "upgraded stretch, ran usrmerge"
19:26:45 <marga> ntyni, it matters for building packages.  If I'm reading the bug correctly, buildds now build with /usr not merged, but anybody else building packages will build with /usr merged, which means that there might be incompatibilities with non-merged systems.
19:26:51 <gwolf> anyway, we are bound to support both setups... no matter what we do
19:27:10 <gwolf> We know and trust non-usrmerged
19:27:25 <gwolf> And I agree, performing this change so close to the freeze is not a winning move :-/
19:27:30 <OdyX> yes, but symlinks can be detected. Wasn't it what dpkg-buildpackage was modified to flag?
19:27:52 <Mithrandir> iirc, it records it.
19:28:45 <ntyni> debootstrap --variant=buildd is non-usrmerged I think (but I'm sure people set up build chroots without that)
19:29:34 <marga> Right, so buildds are non-usrmerged, but individual builds (or even derived distros builds) are usrmerged
19:30:44 <ntyni> yes, for new systems and new chroots without --variant=buildd
19:30:46 <OdyX> provided they were debootstrapped with a debootstrap from buster _before_ it got overriden :-)
19:30:59 <OdyX> (while buster was testing)
19:31:34 <OdyX> https://tests.reproducible-builds.org/debian/issues/unstable/paths_vary_due_to_usrmerge_issue.html is also another way to look at the problem.
19:32:04 <OdyX> 28 packages will build differently; which is not a terrible number.
19:32:19 <Mithrandir> I think we should do what's needed to make buster as good a release as possible and then get the issues fixed properly for buster+1
19:32:22 <marga> What's the 32 number in the first line?
19:32:24 <Mithrandir> but not revert for buster.
19:32:41 <OdyX> marga: used to have the problem, fixed since.
19:32:49 <marga> cool
19:33:03 <OdyX> Mithrandir: what's needed without reverting?
19:33:55 <Mithrandir> OdyX: detect/warn if packages are built on an incompatible system, maybe?  Look at what it'll take to fix the dpkg -S confusion?
19:33:57 <OdyX> [double question: a) are you saying we should not revert/override?; b) what _is_ needed ?]
19:34:47 <Mithrandir> I don't think we should revert if it requires a TC vote to override the maintainer.  I don't mind if people convince the d-i folks to revert. I don't think it solves anything
19:34:54 <gwolf> Mithrandir: FWIW I also don't like the need of reverting, mainly because of the social part. I understand that, reverting or not, the change will affect _something_... (Read: I am clearly still undecided)
19:35:24 <OdyX> another human aspect to keep in mind is that Guillem is not likely to take any TC advice/decision happily (if at all).
19:35:25 <Mithrandir> since we need to support both, the default is not completely unimportant, but it's less important.
19:37:36 <marga> From my understanding of the issue (which is rough), I don't mind the default for new installs to be usrmerged, but I do mind that debootstrap defaults to usrmerged for non-buildds.  This affects anybody building for personal or derived distros purposes.
19:38:00 <Mithrandir> that's a valid point.
19:38:09 <Mithrandir> (and one I hadn't properly appreciated)
19:39:06 <OdyX> it hides another idea too: have a flag-day, through base-files for example, after which all systems become usrmerged.
19:39:43 <OdyX> I don't feel we're ready to get this through in time for buster. It only works if done in buster+1
19:40:05 <marga> For default installs, you mean? Or for build chroots?
19:40:33 <OdyX> all systems. I mean "get all Debian hosts to be usrmerged in buster+1"
19:40:48 <Mithrandir> for buster+1 we can look at how to transition existing systems. We're past transition freeze time, and I think we should respect that and not argue for initiating a pretty big transition at this point in time. :-)
19:40:59 <OdyX> so I can also depend on a version of base-files if I want to make sure that the host has usrmerged.
19:41:02 <Mithrandir> (which, to be clear, I don't think you're doing)
19:41:05 <marga> I think nobody is :)
19:41:44 <OdyX> Mithrandir: oh, absolutely. It's crystal clear we are not gonna get "all buster hosts have merged usr".
19:42:05 <Mithrandir> we've now spent 43 minutes on this, what can we do to move forward in the next couple of days/weeks?
19:42:55 <marga> OdyX suggested having a drafting session on Friday
19:43:11 <OdyX> the last route (option C) is "revert debootstrap, never enforce usrmerge, but make it a release goal that nothing ships in non-usrmerged paths, making non-usrmerged and usrmerged systems equivalent without a flag-day.
19:43:14 <marga> I could make it, but later than the proposed time... 13 CET is the earliest I would be awake to help.
19:43:21 <OdyX> (not for buster either)
19:44:00 <OdyX> perhaps the easiest is I draft "something", to get us started, and commit on the repo; we can iterate there, either all on master, or through MRs.
19:45:20 <Mithrandir> wfm
19:45:37 <ntyni> works for me; no chance to participate on Friday
19:45:40 <gwolf> OdyX: Option C was dismissed as basically unachievable in our naturla lifetimes (i.e. refering to the doc-base transition)
19:45:56 <gwolf> I cannot participate on Friday either, but will take a look at it for sure.
19:46:08 <OdyX> I'm not sure I agree that it's not doable in a release cycle.
19:46:27 <marga> Alright, I guess we have at least consensus on how we move forward.
19:46:52 <marga> #action Odyx to start drafting a proposal on Friday. Others to chime in during/after Friday.
19:47:12 <gwolf> OdyX: If we use TC Powers to set policy as argued earlier, it can be done...
19:47:28 <marga> Let's all try to prioritize this (for example, I'm going to make sure I re-read the whole thing so that I can have good understanding), given that it's time critical to make this decision ASAP
19:47:38 <OdyX> gwolf: whether we should is a different question.
19:48:34 <Mithrandir> let's discuss that later, move on?
19:48:45 <OdyX> yep.
19:49:28 <marga> Ok... Only 10 minutes to spare :-/
19:49:47 <marga> #topic #911225 - Advice on stale libraries in a higher-precedence path entry
19:50:23 <marga> We discussed this in the November meeting I think, and I think we agreed it could be closed... I should find the meeting notes.
19:50:37 <OdyX> I think that was just waiting on smcv to close.
19:51:00 <marga> Yeah, I think the same.  Alright, let's move on to the last one.
19:51:16 <marga> #topic #904558 - What should happen when maintscripts fail to restart a service
19:51:48 <marga> I'm pretty sure this one was waiting on me to summarize the current state of the discussion and then try to move it forward somehow (which is hard, because we don't really agree)
19:52:05 <OdyX> also, it's a hard subject.
19:53:13 <marga> Ok, I think I'll just take the AI again
19:53:34 <marga> Unless someone opposes and wants to take it themselves
19:53:43 <OdyX> Action Item, not Artificial Intelligence, right?
19:53:51 <Mithrandir> please take it,
19:53:51 <marga> Indeed :)
19:54:04 <OdyX> please do yes. :-)
19:54:08 <marga> #action marga to draft a proposal including the parts where we have agreement and ballot options for the parts where we don't.
19:54:22 <marga> #topic Any other business?
19:55:04 <Mithrandir> recruiting efforts? Or are we good?
19:55:20 <OdyX> none besides that it takes big amounts of energy to get back to these topics; winter-season, end-of-year closings and Debian politics don't make it easy.
19:55:21 <marga> We are good until September, I think
19:55:29 <gwolf> I understand we agreed not to care about recruiting so far
19:55:45 <Mithrandir> ok
19:56:06 <OdyX> noone complained (publically) that we were still on the TC Mithrandir, so we're fine I guess :-)
19:56:32 <ntyni> https://www.debian.org/intro/organization is still out of date wrt. the chair, not sure what happened to my bug trying to update it
19:56:52 <marga> Ah, yes, I noticed that today
19:56:53 <OdyX> aww.
19:57:09 <marga> I guess that's good, nobody can complain of how bad a chair I've been :)
19:57:17 <ntyni> :)
19:57:28 <OdyX> well… :D
19:57:29 <ntyni> (also you're good)
19:57:52 <ntyni> guess I'll poke #887557
19:57:58 <marga> Thanks
19:58:03 <marga> Alright, I think we are done
19:58:06 <marga> #endmeeting
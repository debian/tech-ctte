17:08:51 <Diziet> #startmeeting
17:08:51 <MeetBot> Meeting started Thu Jun 26 17:08:51 2014 UTC.  The chair is Diziet. Information about MeetBot at http://wiki.debian.org/MeetBot.
17:08:51 <MeetBot> Useful Commands: #action #agreed #help #info #idea #link #topic.
17:08:54 <Diziet> #topic Who is here?
17:08:56 <Diziet> Ian Jackson
17:09:05 <keithp> Keith Packard
17:09:12 <vorlon> Steve Langasek
17:09:59 <cjwatson> Colin Watson
17:10:17 <Diziet> OK that's everyone who has been speaking recently.
17:10:17 <Diziet> #topic Next Meeting?
17:10:26 <Diziet> Normal schedule would seem to be 24th of July.
17:10:37 <Diziet> Or perhaps 31st.
17:10:56 * bdale is here, but on the phone .. will have full attention here shortly
17:10:58 <Diziet> Oh I see my diary already has 31st pencilled in.
17:11:28 <Diziet> Do we want to schedule a meeting during Debconf and do we want to do that after seeing the DC schedule ?
17:11:29 <vorlon> yes, the 31st seems to already be on the calendar
17:11:31 * bdale is trying to arrange internet service for the new house .. /o\
17:11:42 <Diziet> bdale: Congrats on the new house.  Good luck...
17:11:47 <vorlon> scheduling one during DebConf> well, it's the right time of the month
17:12:02 <Diziet> I think we should deal with that at the next meeting on 31st.
17:12:09 <bdale> Diziet: I haven't done so yet, but was planning to request a TC BOF at Debconf as usual
17:12:18 <Diziet> So next meeting is 31st same time at which we'll discuss timing of a TC mtg.
17:12:30 <Diziet> #action bdale to request TC bof at Debconf
17:12:44 <vorlon> Diziet: sounds fine to me
17:13:05 <Diziet> #agreed Next meeting same time 31st of July
17:13:13 <Diziet> #agreed Discuss time of meeting during Debconf on the 31st
17:13:16 <keithp> as was already in meeting.ics
17:13:21 <Diziet> #topic #717076 Decide between libjpeg-turbo and libjpeg8 et al.
17:13:33 <Diziet> Is there any more to be said about this ?
17:13:39 <Diziet> If not I will just call for a vote after the meeting.
17:14:39 <keithp> Sounds good to me
17:14:43 <Diziet> OK, good.
17:14:48 <cjwatson> I think it's fine.  As I said thanks for picking up the ball I dropped.
17:14:52 <Diziet> NP
17:14:53 <Diziet> #topic #636783 constitution: super-majority bug
17:14:59 <Diziet> Oh I forgot
17:15:05 <Diziet> #agreed Diziet to CFV on #717076
17:15:17 * bdale is ready to vote on the jpeg issue
17:15:41 <Diziet> I doubt anyone has had much of a chance to read my text in detail.
17:15:49 <Diziet> And we should leave some time for -vote to comment.
17:16:06 <Diziet> My main question here is the grouping one.  Which of these changes can be combined.
17:16:45 <bdale> I saw that there was an email with you on this, glanced quickly, but haven't had time to read it yet or consider, so no opinion from me yet on groupability
17:16:57 <Diziet> OK
17:18:01 <KGB-3> 03Ian Jackson 05master 9ca30e7 06debian-ctte 10meetings/agenda.txt meetings/agenda.txt: add two missing constitution items
17:18:01 <KGB-3> 03Ian Jackson 05master 77aabaa 06debian-ctte Merge branch 'master' of git+ssh://git.debian.org/git/collab-maint/debian-ctte
17:18:08 <Diziet> Let's go onto the other issues quickly then.
17:18:13 <Diziet> #topic #636783 constitution: casting vote
17:18:40 <Diziet> It seems that there is quite some opposition to involving the DPL.  We should probably just go with expanding the TC to 9 then.
17:18:49 <Diziet> Unless that is also controversial.
17:19:07 <cjwatson> Russ wasn't too keen on it in comment #200, apparently
17:19:37 <cjwatson> I disagree with him on this; while mathematically it's true, my feeling is that most of these kinds of issues (god forbid we have more ...) are likely to devolve to two competing standpoints
17:20:04 <Diziet> Also he is just wrong.  Our rules don't permit the casting voter to deal with condorcet cycles; the Schwarz sequential dropping is done first.
17:20:14 <Diziet> So you still only use the casting vote when there's ties.
17:20:38 <Diziet> "No (unequal) defeats within the Schwartz set"
17:20:56 <bdale> I like the idea of expanding the ctte to 9 in any case, fwiw
17:20:57 <Diziet> I should say this in email really.
17:21:01 <Diziet> OK.
17:21:24 <Diziet> #agreed Move forward with proposal to expand to 9.  (agreed amongst those present)
17:21:35 <Diziet> #topic #636783 constitution: minimum discussion period
17:21:35 <keithp> 9-member ctte resolves the simple problems in a simple way, without radically changing the dynamics
17:21:53 <bdale> keithp: right, my feelings too
17:22:02 <Diziet> I know we don't have consensus on this.  And I have just sent a proposal, so people probably want to read it.
17:22:10 <bdale> yes, I'd like to read first
17:22:15 <Diziet> OK.
17:22:21 <Diziet> #topic #636783 constitution: TC member retirement/rollover
17:22:50 <Diziet> We seem to be agreed that this is a good principle and are arguing about details on -project.  AJ and I have proposals.
17:23:21 <bdale> I'm not current on -project
17:23:40 <Diziet> Just a handful of TC-related messages in the past few days.
17:23:49 <Diziet> I've been trying to remember to CC #636783
17:23:50 <bdale> ok, will try to catch up later today
17:24:12 <Diziet> No-one seems to be objecting to the direction, anyway.
17:24:16 <Diziet> #topic #636783 constitution: TC chair retirement/rollover
17:24:18 <vorlon> I'm in favor of limiting terms, but ultimately this has to be decided by the project rather than by the CTTE so I haven't felt the need to stick my nose in
17:24:24 <Diziet> vorlon: Right.
17:24:37 <Diziet> #topic #636783 constitution: TC member retirement/rollover
17:24:48 <bdale> discussed in the same thread?
17:24:53 <Diziet> vorlon: Do you think it's right for the TC to sort out the details and propose the final text ?
17:24:57 <Diziet> bdale: Mostly, yes.
17:25:18 <vorlon> Diziet: if the TC doing this doesn't require my active involvement, sure ;)
17:25:24 <Diziet> vorlon: Heh.
17:25:36 <Diziet> The TC chair thing I have only just posted although iirc I mentioned it on irc last month.
17:25:54 <bdale> I see the thread, haven't read it yet
17:25:56 <Diziet> Does anyone (notably, bdale) have any objection to the principle or any comments ?
17:26:10 <bdale> since, as I said, I'm not up on -project atm
17:26:14 <Diziet> The principle being that the TC chair should be reappointed occasionally (eg when a new member joins the TC, or periodically).
17:26:28 <Diziet> The TC chair thing I posted to #636783 but only recently.
17:26:28 <bdale> I agree in principle, yes
17:26:31 <Diziet> OK, good.
17:26:38 <Diziet> I think we can sort out the details by email.
17:26:45 <bdale> seems likely
17:27:25 <Diziet> #agreed TC member retirement good in principle, details to be worked out on -project
17:27:39 <Diziet> #agreed TC chair retirement/reelection good in principle, details to be worked out in email
17:27:44 <Diziet> #topic #681419 Depends: foo | foo-nonfree
17:28:02 <Diziet> We've now had opinions from vorlon and myself who seem to be the opposite corners.
17:28:10 <bdale> I've read both
17:28:11 <Diziet> I don't think I have anything to add.
17:28:13 <vorlon> I don't think there's anything more to discuss on this, just voting :)
17:28:21 <bdale> i agree, we can vote on this
17:28:33 <keithp> concur
17:29:04 <ansgar> Diziet: Your last mail on this didn't make a difference on foo | foo-nonfree, foo | foo-external and foo | foo-no-longer-existing, or did I miss that part?
17:29:09 <Diziet> vorlon: Does the text in A of cjwatson_draft.txt express everything you think it needs to ?
17:29:55 <vorlon> checking
17:29:57 <Diziet> ansgar: Uh ?  I'm not sure what your point is about foo-no-longer-existing.  The problem is with mentioning the names of nonfree programs.  There is no problem with mentioning the names of free packages that are simply gone.
17:30:22 <Diziet> vorlon: The B text LGTM.
17:30:31 <ansgar> Diziet: Ah, I read the "external/nonfreefoo" as "anything not in main".
17:30:39 <vorlon> Diziet: yes, I'm happy with the A text
17:30:48 <Diziet> vorlon: OK then, shall action you to CFV ?
17:30:54 <vorlon> sure
17:30:56 <cjwatson> OK, in that case I miraculously did a good job of representing both positions and we should just vote :)
17:31:06 <Diziet> #action vorlon To CFV on cjwatson_draft.txt, A vs B vs FD
17:31:12 <Diziet> #topic #741573 menu systems and mime-support
17:31:35 <Diziet> keithp: Did you intend your draft to be ready ?  Because as I say in email I think there are some loose ends.
17:31:54 <Diziet> cjwatson: PS well done :-).
17:32:23 <keithp> Diziet: I was looking for feedback on the resolution bits; the informative bit at the end is not relevant to that, of course
17:32:38 <Diziet> keithp: OK.  So you want to keep refining that.
17:33:27 <keithp> Diziet: I'll clarify what I expect a transition plan to look like, in particular
17:33:46 <Diziet> #agreed Continue to discuss keithp's one-menu-system text in email.
17:33:50 <Diziet> keithp: OK, good.
17:33:51 <Diziet> Thanks.
17:34:04 <Diziet> Is there anything else we need to say about this question here ?
17:34:08 <keithp> the basic gist is that the 'menu' package should read .desktop files as well as menu files; that gives a transition that works, I think
17:34:15 <bdale> keithp: thanks for getting a draft done before this meeting, btw
17:34:39 * keithp implemented macosx/windows/.desktop/.menu systems for one application to figure out how they worked :-)
17:35:18 <Diziet> Shall we move on ?
17:35:24 <bdale> yes
17:35:26 <Diziet> #topic #746715 init system fallout
17:35:55 <bdale> I think I replied to this one in email already?
17:35:57 <Diziet> Right.
17:36:09 <Diziet> So unless anyone has any objections I will CFV after the meeting.
17:36:14 <bdale> works for me
17:36:50 <Diziet> #action Diziet to CFV on #746715 with text as sent last night
17:36:59 <dondelelcaro> sorry everyone; watchimg the us game with almost no network
17:37:03 <Diziet> dondelelcaro: Ah hello.
17:37:08 <Diziet> #topic #750135 Maintainer of aptitude package
17:37:16 <Diziet> dondelelcaro: Hope you don't mind me usurping your role...
17:37:31 <Diziet> We have a fundamental problem with these "problem maintainer" complaints.
17:37:37 <dondelelcaro> no; thanks for running the meeting
17:37:44 <Diziet> We have been struggling to find a good approach to dealing with them.
17:38:02 <Diziet> Last night I had a brainwave: how about we have a more formal process ?
17:38:07 <cjwatson> util-linux is kind of the same.
17:38:10 <Diziet> Yes.
17:38:21 <cjwatson> (For which I'm conflicted because I know the maintainers, but anyway.)
17:38:41 <Diziet> Eg when we get a report like that we would see if there seemed to be a case to answer, and if so we would publicly solicit references to publicly documented interactions with the maintainer.
17:39:57 <Diziet> After a defined period of time we would decide between (a) the existing maintainer should carry on (b) the existing maintainer needs help (c) the existing maintainer needs to be replaced.  In cases (b) and (c) we would then invite further comments including volunteers.
17:40:54 <Diziet> (publicly solicit references> Which perhaps might be sent by private email to a TC member who would forward the reference but not the identity of the forwarder.)
17:41:00 <Diziet> Or some such.
17:41:41 <keithp> Diziet: would that be better than the current "process" of grubbing through existing email threads for instances of such interaction?
17:42:08 <Diziet> keithp: At least it would avoid us having to do so much of the grubbing.
17:42:17 <Diziet> And it makes the process more open.
17:42:29 <cjwatson> We also need to make sure that there's a clear avenue for the maintainer to respond; in past cases we've seen cases where people felt that the process was a bit of a lose-lose for them
17:42:39 <keithp> soliciting for additional information would be good; I think I'd still end up searching on my own and adding to the list
17:42:59 <cjwatson> (I have about 10-15 minutes.)
17:43:09 <Diziet> Also separating out "is there a problem" from "who is volunteering to help" will avoid the problem that no-one wants to volunteer and then find the TC leaves the existing maintainer in place (a maintainer who will now think ill of them).
17:43:19 <Diziet> cjwatson: Indeed
17:43:25 <dondelelcaro> i think at least asking for patches and specifics is a good first step
17:43:29 <keithp> cjwatson: do we want some kind of real-time tribunal to let the maintainer address the TC?
17:43:32 <Diziet> If this seems like a good general idea I will write up something specific, and hopefully we can try it out in these two cases.
17:43:39 <bdale> we've also suffered when the maintainer felt there was nothing good that would come from commenting
17:43:46 <cjwatson> I notice that Daniel Hartwig hasn't responded yet
17:43:48 <Diziet> keithp: I think it would be a good idea to invite the maintainer to do so.
17:43:54 <cjwatson> bdale: Right, that's pretty much what I meant
17:44:24 <cjwatson> keithp: Maybe.  Some people do very badly in that kind of situation in ways that don't reflect on their qualities as maintainers
17:44:29 <cjwatson> But maybe it should be an option
17:44:30 <bdale> without naming names, at least 3 other of our toughest problems in this case involved maintainers who were technically strong but where communication style issues caused problems
17:45:02 <vorlon> 3?
17:45:03 <bdale> so, while I'd love to believe putting a more regular process in place would actually help us here, I'm not sure I believe it's really going to help
17:45:03 <Diziet> I would like to explicitly say that a maintainer can email is privately.
17:45:22 <bdale> vorlon: I mean 3 previous complaints about maintainers of different packages
17:45:33 <vorlon> bdale: yes, I'm trying to think of more than 1
17:45:41 <cjwatson> The thing I'm always cautious of in these kinds of situations is that I think one of a maintainer's most important and difficult jobs is saying no
17:45:49 <cjwatson> (In appropriate cases!)
17:45:49 <Diziet> cjwatson: Indeed.
17:45:58 <Diziet> So I don't think "saying no" is EBW.
17:46:09 <keithp> Diziet: yeah, some way of letting the maintainer contribute without leaving them exposed
17:46:10 <Diziet> But not saying anything is a problem.
17:46:34 <cjwatson> keithp: well put
17:46:37 <bdale> communication mediation is clearly a place where "we" can help
17:46:57 <Diziet> Indeed saying "yes" or "no" is basically the one thing a maintainer _can't_ delegate (passively or actively).
17:47:05 <keithp> ctte counseling services
17:47:10 <bdale> more or less
17:47:47 <bdale> in reality, it's a lot of what we end up doing on a lot of the issues brought to us... in the past, many cases seem to me to have been as much about communication breakdowns of one kind or another as any other root cause?
17:48:04 <Diziet> Yes.
17:48:41 <cjwatson> Clawing back to the aptitude case, the ... plaintiff? ... has already presented us with some specifics; I guess we need to see some examples of disputed patches which we can consider, and hear the maintainer's side of the story
17:48:41 <bdale> Diziet: I recall you having explicit mediation ideas in the past, which we've talked about in Debconf TC BOFs, etc
17:48:58 <Diziet> bdale: Yes.
17:49:08 <Diziet> It's difficult because of the no-private-discussion rule.
17:49:10 <bdale> cjwatson: yes, it's far easier to tackle these things when we can think/research in terms of patches that are in question than generalities
17:49:44 <bdale> Diziet: hrm .. you've mentioned that "rule" before .. remind me what you think the basis of that is?
17:50:01 <Diziet> 6.2(3)
17:50:11 <Diziet> contrast (4)
17:50:17 <cjwatson> ITYM 6.3(3)
17:50:21 <Diziet> Err yes
17:51:08 <bdale> so, sure, I personally interpret that to mean that we don't make any "decisions" in private, but I've never believed that meant we couldn't have private conversations
17:51:27 <keithp> bdale: it does include 'discussion'
17:51:40 <keithp> which seems pretty broad
17:51:58 <Diziet> Hence 636783_supermajority/propose-informal
17:52:07 <bdale> but discussion has a meaning in the context of our decision making context, I don't believe that word in that para was meant to mean we can't sit down over lunch and have a chat without recording gear
17:52:21 <bdale> if so, it's ludicrous
17:52:40 <Diziet> Some people interpret it that way.
17:52:40 <cjwatson> Oh, you interpret "discussion" as in A.1(1)?
17:52:54 <cjwatson> Constitution needs more capitals.
17:52:56 <bdale> yes
17:53:16 <Diziet> Leaving that aside, I think in practice we won't get pushback if we more explicitly invite private commentary about maintainership.
17:53:24 <bdale> I've always assumed "discussion" in this context mean the process leading to a ballot
17:53:40 <bdale> which isn't at all the same thing as talking about a pending issue, conversing with people to find out what they think and what we can do, etc
17:53:52 <bdale> I understand that others might interpret this differently
17:54:14 <bdale> so I'm not *negative* about the propose-informal thing, I just don't think it is necessary, or should be necessary
17:54:41 <bdale> Diziet: I agree
17:54:56 <Diziet> Do we think it's a good idea for me to propose some kind of structure for answering #750135 and #752400 ?
17:54:58 <cjwatson> I expect it'll be pretty uncontroversial and we might as well do it to eliminate doubt.
17:55:03 <cjwatson> (propose-informal)
17:55:20 <keithp> Diziet: so, I think in the specifica case of aptitude, we should invite a private statement from the current maintainer, to be shared amonst the ctte but not published
17:55:24 <Diziet> Or to put it another way, does anyone have a better idea for how to go about answering the questions ?
17:55:31 <Diziet> keithp: OK.
17:55:40 <vorlon> well, having looked at the issue a bit deeper during the course of this meeting
17:55:49 <vorlon> it's not clear to me that Daniel is "current maintainer" anyway
17:55:52 <cjwatson> Perhaps we can do these two in series rather than in parallel so that we can see how it works ...
17:56:13 <keithp> vorlon: it is pretty murky
17:56:22 <vorlon> both Daniel and Manuel are marked as uploaders on the package; Daniel has apparently revoked Manuel's commit access to the upstream repo but not changed the uploaders field
17:56:31 <bdale> ick
17:56:52 <vorlon> so while it's appropriate for the TC to arbitrate, it doesn't look like one is more of an incumbent than the other
17:57:00 <cjwatson> and it's a non-native package so upstream access doesn't necessarily mean packaging access
17:57:17 <dondelelcaro> right
17:57:21 <Diziet> vorlon: Would you mind putting that in an email ?
17:57:22 <dondelelcaro> im nore
17:57:43 <vorlon> Diziet: ack
17:57:49 <cjwatson> So in this case we should probably regard the two as disputants rather than putting one on a pedestal.
17:57:50 <dondelelcaro> concerned about the non public work that is apparently happening
17:58:09 <Diziet> in series> Is either of these urgent ?  util-linux might be urgent if we want a new version in jessie.
18:00:25 <vorlon> both are important packages that need to be well cared for, but I don't think the TC treating them as "urgent" will necessarily improve the outcome
18:00:52 <bdale> I don't know that either is "urgent", but I hate to let inter-personal maintainer issues fester if it's getting in the way of functionality we'd like in our next release
18:00:56 <cjwatson> I have to go I'm afraid.  Handed the childcare baton while Kirsten goes to karate.
18:01:11 <Diziet> OK then.  So I should propose some kind of process which we will refine and/or try out on #750135 and revise/drop/use for #752400 ?
18:01:27 <Diziet> cjwatson: Thanks for your contributions.
18:01:28 <Diziet> ttfn
18:02:04 <Diziet> process> Including the maintainer perhaps providing private response to us, and also perhaps an irc conversation ?
18:02:18 <bdale> sure, I don't think we have anything in particular to lose by trying!
18:02:52 <Diziet> #agreed Diziet should propose some kind of process which we will refine and/or try out on #750135 and revise/drop/use for #752400.  Including the maintainer perhaps providing private response to us, and also perhaps an irc conversation.
18:03:03 <Diziet> Now we have two dupes in the agenda (which we dealt with earlier).
18:03:05 <Diziet> and
18:03:08 <Diziet> #topic Additional Business
18:03:37 <Diziet> Anything else ?
18:03:56 <vorlon> not here
18:04:13 <bdale> I'm good.  Thanks for running the meeting, Diziet.
18:04:19 <keithp> thanks guys
18:04:23 <Diziet> NP.  Thanks everyone.
18:04:26 <Diziet> #endmeeting
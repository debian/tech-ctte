18:00:57 <OdyX> #startmeeting
18:00:57 <MeetBot> Meeting started Thu Jul 28 18:00:57 2016 UTC.  The chair is OdyX. Information about MeetBot at http://wiki.debian.org/MeetBot.
18:00:57 <MeetBot> Useful Commands: #action #agreed #help #info #idea #link #topic.
18:01:04 <ScottK> hartmans: https://ftp-master.debian.org/REJECT-FAQ.html lists "compressed .js libraries" as not source.
18:01:18 <ScottK> So I think that's a yes.
18:01:25 <OdyX> #topic Check-in round
18:01:31 * marga here
18:01:32 <hartmans> Sam hartman
18:01:36 <aba> yep
18:01:39 <dondelelcaro> Don Armstrong
18:01:52 <OdyX> Didier Raboud
18:01:57 <marga> uhm, ok, Margarita Manterola, I guess
18:02:10 <hartmans> It helps to give people reading the logs a mapping from nic to name
18:02:14 <OdyX> It's a round of names, to record who's here.
18:02:49 <keithp> Keith Packard
18:03:32 <OdyX> So we're waiting on fil & Mithrandir
18:04:16 <aba> looks like
18:06:36 <marga> How long do we normally wait?
18:06:45 <keithp> not longer than this
18:06:51 <OdyX> dat.
18:06:52 <OdyX> :)
18:06:59 <OdyX> #topic #830344: How should the TC help with a project roadmap?
18:07:12 <OdyX> So the status is that some of us have sent their opinions.
18:07:21 <OdyX> I still need to send mine.
18:07:30 <OdyX> But "coming back to work" has hurt.
18:07:49 <aba> I'm happy I'm not the only one with that :)
18:08:11 <aba> I hope to send mine next week
18:08:16 <hartmans> Do we need to summarize this or will the DPL be able to get an idea about how we could contribute just from this?
18:08:17 <keithp> I had a week of debconf plague on returning...
18:08:38 <hartmans> keithp:  I mostly only had that at debconf:-)
18:08:50 <jcristau> we blame dondelelcaro for all the debbugs
18:08:52 <hartmans> But it was fairly mild in my case.
18:09:02 <aba> I just got sick ears by virtue of pressure changes during flight
18:09:28 <Mithrandir> hi, sorry I was slightly late.
18:10:13 <keithp> hartmans: back on topic, we've been asked for a group opinion on how the TC can help, so it seems like we should probably have something that we have rough consensus around?
18:10:55 <OdyX> I'd like to delegate a champion for this issue (and any other issue), a fighter trying to identify consensus
18:11:02 <dondelelcaro> beyond helping to facilitate discussion, I don't know what the TC can do to help
18:11:11 <hartmans> Does someone want to take the action to prepare a draft of that from our input--say giving people another week to give input and a week after that to get a draft summary?
18:11:18 <marga> I'm still rather confused by the fact that there was a proto-roadmap-team formed during the roadmap bof, while the DPL was saying that he wanted the TC to be the roadmap team
18:11:18 <hartmans> I am not volunteering for that one.
18:11:48 <marga> I can volunteer for that, but first we need to have had opinions
18:11:57 <dondelelcaro> potentially anyone could be part of the roadmap team; they don't have to be serving on the TC
18:12:03 <aba> I don't think we are yet at a consensus
18:12:09 <keithp> aba: agreed
18:12:13 <aba> also, roadmap items shouldn't be RC
18:12:24 <aba> (unless they also violate the rc policy by the release team)
18:12:27 <keithp> aba: I agree with that too :-)
18:12:51 <marga> Are we going to discuss the topics now, or are we going to set a time line to discuss them on the bug?
18:13:05 <OdyX> For me, we're debating at least three axes: what the roadmap is (or should be), how should arbitration work around the roadmap items, and whether the TC should be the arbitration body.
18:13:12 <aba> marga: I'd say 5 minutes or so now, and rest later. But we'll see
18:13:25 <OdyX> And the three items are of increasing relevance regarding our opinion.
18:13:30 <aba> OdyX: s,whether,how much,
18:13:59 <aba> IMHO the tc will be the final arbitration body in case of real escalations.
18:14:18 <mehdi> OdyX: Indeed. I agree with those three questions and you can think about them independently of the proto-roadmap-team that has been mentioned during the BoF.
18:14:22 <aba> but most cases should rather be finished at other lines
18:15:29 <mehdi> My opinion on that (if I may) is that roadmap items are long tasks proposed by project members on which they can commit (somehow, somewhen). The roadmap is first a way to promote their work and shed more light on it.
18:15:32 <OdyX> So, for things we can discuss hic&nunc, I'd like to designate keithp as champion for this subject, and ping all those who haven't formulated their opinions to post them on the bug.
18:15:37 <Mithrandir> I agree we should be the escalation body for conflicts rather than first port.
18:15:38 <marga> In my view, the Roadmap team is a facilitating team that gathers goals, curates them using some criteria and then presents them to the developers. I don't think this needs to be the TC at all.
18:15:47 <Mithrandir> (I don't think most roadmap items will be controversial though)
18:16:13 <keithp> marga: we may want to monitor the results and offer opinions on them though
18:16:20 <aba> marga: yes, actually to help them to get work done, and not to stear/control what they are allowed to do
18:16:33 <marga> I didn't say the TC wouldn't be involved
18:16:42 <marga> But rather that the TC doesn't need to be the Roadmap team
18:16:46 <keithp> marga: right, the question is whether we should be reactive or proactive, I think
18:16:47 <mehdi> Indeed. I don't expect controversial proposals. But the tech-ctte is (IMHO) the best body to debate on some conflicting ideams and set a direction.
18:16:50 <aba> looks like we have mostly a consensus here
18:16:53 <marga> We can be involved as advisors, yes.
18:16:59 <keithp> agreed
18:17:16 <aba> mehdi: anyone (including the roadmap team) could ask us for our opinion if helpful
18:17:34 <OdyX> We're all implying the Roadmap Team is not the TC (but can be made of TC members)
18:17:52 <Mithrandir> OdyX: yes, I think that makes sense.
18:18:07 <hartmans> I don't entirely agree with this but have stated my opinions often enough that I don't think restating them would help.
18:18:20 <mehdi> There are other important arguments to have the TC handling this: 1) Constitution forces the team to renew; 2) Constitution allows TC to decide on technical matters; 3) We have already so many teams.
18:18:20 <keithp> OdyX: I think that makes the most sense; the TC as a body is not supposed to do development work, and the roadmap activity seems like development work to me
18:18:33 <OdyX> I'd even go as far as saying it's important for TC members to get involved.
18:19:00 <hartmans> I'd ask you to think about the late-surprise issue I've brought up especially as that's the major thing I've brought up that I don't think I've seen responded to or incorporated.
18:19:27 <keithp> hartmans: I'm not sure I know what you're referring to
18:19:28 <aba> OdyX: I'd put it as "it's important for TC members to be involved at relevant discussions in the project, and this is one"
18:19:52 <hartmans> keithp:  discussed both in person and in my write-up to the bug.
18:19:54 <Mithrandir> (we're now past 5 minutes)
18:20:19 <aba> hartmans: I'll send a reply to your mail. Not tonight but this weekend or so
18:20:26 <OdyX> Yeah. What about the "keithp as champion" ?
18:20:31 <aba> go for it :)
18:20:54 <OdyX> #action all TC member who haven't mailed their extensive opinion should do so in the next week.
18:21:20 <keithp> OdyX: I accept
18:21:24 <OdyX> keithp: ack, nack ? (you can #action yourself)
18:21:50 <keithp> #action keithp to champion the TC's response to 830344
18:21:59 <OdyX> #topic Approval of the Minutes of the 2016-07-03 Breakfast Meeting
18:22:17 <hartmans> For this topic I just want to confirm from those present that the minutes I sent out are accurate.
18:22:32 <hartmans> I'm assuming it would be reasonable if they are to stick them into git in the usual place.
18:22:49 <hartmans> The next topic is discussing whether we agree with what was discussed there.
18:22:59 <OdyX> I agree.
18:23:00 <keithp> hartmans: your minutes seem accurate with my recollection
18:23:21 <Mithrandir> as I followed up to the list, they pretty much match my recollection.
18:23:26 <hartmans> Okay, if there are no objections I'll check them into git.
18:23:31 <keithp> tnx
18:23:51 <hartmans> #action hartmans to check in breakfast meeting minutes.
18:25:19 <OdyX> # topic 2016-07-03 Actions
18:25:22 <OdyX> #topic 2016-07-03 Actions
18:25:49 <OdyX> So hartmans you had multiple points here.
18:26:00 <hartmans> Right.
18:26:15 <OdyX> #topic Actions - Actions without votes
18:26:17 <hartmans> So, I wanted to get input from dondelelcaro and marga who weren't at the breakfast meeting on major discussion points there
18:26:29 <hartmans> And to see if others had any concerns since that meeting about what we discussed there.
18:27:09 <hartmans> this is just a final chance for people to raise concerns.
18:27:34 <OdyX> Your proposal was " votes aren't needed when actions can be undone easily and don't involve exercise of constitutional power like overriding a maintainer."
18:27:44 <dondelelcaro> I disagree
18:28:06 <keithp> dondelelcaro: I think the question was how long should we wait for someone to ask for a vote in these cases?
18:28:08 <marga> dondelelcaro, with what in particular?
18:28:19 <dondelelcaro> I think that having votes is an easy way to get everyone to weigh in on an issue so that the issue can actually be resolved
18:28:47 <dondelelcaro> it just records what the consensus is
18:28:58 <OdyX> The test-case could be fil's recent closing of the multiarch bug.
18:29:08 <dondelelcaro> if the consensus is clear, then the voting is a formality, which is pretty trivial
18:29:21 <OdyX> (which, FTR, we withouth-vote-agreed in private)
18:29:30 <aba> dondelelcaro: do we need a vote to close a obvious to be closed bug?
18:29:58 <ScottK> Surely you need something more than agreed in private?
18:30:01 <dondelelcaro> aba: the vote can just be as simple as: I call for a vote to close this bug, I vote for closing it, and it's closed.
18:30:08 <keithp> dondelelcaro: would it be acceptable to simply re-open the bug and ask for a vote if you weren't able to respond to the request for comment in time?
18:30:17 <dondelelcaro> aba: and if no one objects, then it'll stay closed
18:30:23 <hartmans> dondelelcaro:  Note that several people on the TC have indicated that we find the votes you're asking for demotivating.
18:30:39 <keithp> hartmans: not the vote, but the lag
18:30:48 <keithp> i think?
18:30:54 <OdyX> ScottK: to be clear: we discussed the opportunity of closing in an IRC meeting, discussed a vote proposal in private, and pushed fil to do a bug-close on the line of decision-naking we're currently discussion.
18:31:01 <hartmans> If you can make clear measurable forward progress in a short period of time, it's much easier to get excited about working on a project.
18:31:13 <ScottK> OK
18:31:14 <Mithrandir> I feel like doing a vote adds a lot of threshold to actions.  Do we want that threshold even when they're easily undone?
18:31:34 <dondelelcaro> I guess I just don't think that a vote is a huge threshold, because they're really easy to hold
18:31:45 <dondelelcaro> it just takes a message, and you'd have to send an e-mail to close the bug
18:31:54 <keithp> dondelelcaro: how about 'I closed this bug, please vote if you want it reopened'?
18:32:07 <OdyX> (and recording them in the git for devotee's purposes is mostly an exercise for cases that are uncontroversial.
18:32:16 <hartmans> dondelelcaro:  If you call for a vote without making sure you get all the ballot options you risk systemd all over again, or create undue influence on the process.
18:32:54 <dondelelcaro> hartmans: well, either you're closing the bug in a way that is totally oncontroversial, or you aren't.
18:33:15 <keithp> in which case a 'close/FD' ballot seems like it should be sufficient?
18:33:28 <hartmans> The threshhold for reopening a bug is a lot lower than convincing people to vote fd so you can get another option on the next ballot.
18:34:03 <dondelelcaro> but I guess this doesn't really matter to me so long that when the bug is closed, it is made very clear that anyone who wants to see an actual vote can reopen it
18:34:16 <dondelelcaro> (and anyone, as in, they don't have to be a TC member)
18:34:18 <hartmans> Also, if people are holding votes quickly without making sure they have the right ballot, you can get a situation where the election is no longer in doubt before you've even had a reason to raise an opinion and get it considered.
18:34:23 <OdyX> For me, it's fine to exercise fine judgment for simple closures when its uncontroversial. For other cases, a vote is good to cristallize the varied opinions, iff we can get a meaningful ballot.
18:34:37 <Mithrandir> dondelelcaro: that'd be fine with me, having anybody in the project being able to go "no, stop".
18:34:38 <aba> so, how do we continue here? I tend to think we should close bugs in total uncontroversial topics, but well.
18:34:58 <dondelelcaro> Mithrandir: yes, that could work
18:34:59 <hartmans> dondelelcaro:  that works for me.
18:35:23 <dondelelcaro> lets just do that then; if it's really uncontroversial, then it'll stop us from getting bogged down
18:35:38 <dondelelcaro> and if it is controversial, then we can actually take the time to resolve the issue
18:35:39 <OdyX> Do we have an #agreed
18:35:46 <OdyX> ?
18:35:47 <dondelelcaro> does anyone object?
18:35:48 <Mithrandir> dondelelcaro: sounds good to me.
18:35:52 <marga> sgtm
18:36:03 <aba> good
18:36:17 <keithp> +1
18:36:23 <Mithrandir> OdyX: something like, uncotroversial isuses can be closed, anybody in the project can stop+reopen for full discussion.
18:36:52 <OdyX> #agreed Bugs really uncontroversial just just be closes, anybody in the project can obviously object, thereby triggering a concrete vote.
18:36:53 <Mithrandir> (if we misjudge that it's uncontroversial)
18:37:00 <OdyX> #save
18:37:05 <Mithrandir> ++
18:37:21 <OdyX> #topic Actions - Email Response Time
18:37:42 <hartmans> we wanted to establish an expectation that people would generally read TC mail within a week
18:37:45 <OdyX> hartmans' proposal was "Increasing Bar for Objections as time progresses"
18:37:50 <marga> I agree with the 1 week response time and giving explicit ack
18:37:55 <hartmans> and that we understand that especially the longer you wait, the higher the bar for raising objections.
18:38:11 <marga> Also with that
18:38:28 <dondelelcaro> seems reasonable; I'd just like to request that e-mails that you expect an ack on be short and obvious what you'd like an ack on
18:39:13 <dondelelcaro> the meeting notes were great, but it was still hard for me to extract out the separate topics and to see what was actually wanted
18:39:20 <OdyX> Yeah. I fail at the 1week reading threshold (especially post-DebConf), but I'm fine with the process to continue without me in that case.
18:40:53 <dondelelcaro> (I know I personally fall into "TL;DR" mode very quickly)
18:42:22 <marga> #agreed There is an expectation of 1 week for replies. Explicit acks are preferred over silent acceptance. Mails should be clear when and about what they need an ack. Objections should be raised as early as possible, the bar for objections increases as time passes.
18:42:42 <hartmans> so, some of the most important things to get acks on are summaries of things like menu systems etc.
18:43:01 <hartmans> I'm working on getting summaries shorter, but it's hard when summarizing 50+ messages to get it too short.
18:43:01 <OdyX> #save
18:43:11 <OdyX> #topic #830978: Browserified javascript and DFSG 2
18:43:27 <OdyX> That's our big thing for today, and we're 45 minutes in the meeting.
18:44:31 <marga> There's been a lot of interesting discussion on this, and I've done a lot of thinking on my own
18:44:38 <OdyX> So. I haven't participated in the mail discussion, but mostly in IRC discussions.
18:44:45 <keithp> I had some questions on this one -- the first is what form 'upstream' expects patches to be in.
18:44:53 <marga> I think there is rough consensus that it's not the TC's job to decide what's source code and what is not source code.
18:45:09 <OdyX> The ack
18:45:12 <OdyX> ack
18:45:27 <Mithrandir> I have two questions, at least, one is what we're actually being asked and one being how broad we want the answer to be.
18:45:32 <hartmans> I've been trying to put together a draft resolution for discussion.
18:45:40 <keithp> marga: I disagree, and think that the TC needs to mediate between the developer and FTP masters in this case
18:46:38 <marga> keithp, do we? The maintainer has already agreed to move stuff to non-free.
18:46:46 <OdyX> I feel the FTP masters have a quite clear policy out, but haven't been following on the precise bug after lfaraone reporting.
18:46:53 <hartmans> keithp:  Why do you favor that over suggesting the developer could directly talk to ftp?
18:47:19 <hartmans> Also, asa strict process issue, ftp doesn't have a contraversial issue on the table.
18:47:27 <aba> the spell seemed to have worked ...
18:47:40 <hartmans> In that ftp has not responded to severity serious->wishlist.
18:48:01 <aba> keithp: I think we could offer mediation if wanted - I'm not exactly sure what I really wanted here
18:48:08 <hartmans> From a process standpoint, there seem to be some people in pkg-javascript-devel who want severity serious and some in pkg-javascript-devel who want wishlist.
18:48:15 <keithp> hartmans: I haven't read through the follow on discussions as much as I should
18:48:18 <OdyX> I'm just uneasy taking that inaction as an explicit decision.
18:48:44 <hartmans> okay. let me summarize
18:48:54 <hartmans> 1) Luke files bug.
18:49:23 <hartmans> Pirate responds that browserified is source
18:49:28 <hartmans> and sets severity to wishlist
18:49:47 <hartmans> 2) Jonas and some others who may be pkg-javascript-devel but who are clearly not ftp disagree and there's severity edit war
18:49:53 <hartmans> 3) Pirate files tc request
18:50:11 <hartmans> 4) ansgar files a different RC bug indicating that on top of everything else the source is parser output.  No one has disputed that.
18:51:07 <aba> great.
18:51:16 <keithp> hartmans: I just reviewed the bug to see if I had missed anything -- the message indicating the move to non-free included an explicit request for a decision from the TC
18:51:18 <hartmans> luke or ftpmaster in general may well wade in on browserified javascript (which no one has defined), but I think a lot of folks would like to see whether after the parser output is resolved, whether it's just concatenated javascript files or something more.
18:51:36 <OdyX> It's quite obvious by now, for the project at large, that the precise source package is non-free, right?
18:51:49 <dondelelcaro> OdyX: yeah, I think so. It's got a Jison generated parser in it
18:51:58 <aba> OdyX: because of the parser output, yes
18:52:13 <hartmans> keithp:  Two questions have been asked of the tc
18:52:24 <hartmans> well actually 3
18:52:31 <hartmans> thanks to Don to getting clarification on this
18:52:34 <OdyX> I wouldn't like us to decide on an old version of the package, it's not interesting for the project, I feel.
18:52:39 <hartmans> 1) 6.1.5 advice on what javascript is.
18:53:02 <hartmans> 2) override release team's decision to classify the bug as rc.  Except it's not clear we can do that and besides release team hasn't made that decision
18:53:17 <hartmans> 3) iwj asks us to summarize the general issue to help guide future discussions.
18:53:36 <helmut> ad 3) like perl
18:53:37 <aba> hartmans: for 2, I'd refuse to overrule.
18:53:48 <keithp> aba: agreed
18:53:55 <OdyX> I don't really like wide blanket statements (aka 3))
18:54:00 <keithp> OdyX: also agreed
18:54:12 <hartmans> OdyX:  I think making statements like 3 is our primary value:-)
18:54:23 <Mithrandir> I like blankets, though.
18:54:28 <Mithrandir> and pigs in blankets.
18:54:32 <keithp> case law is built by looking at specific instances, not by constructing theories and then applying them though
18:54:33 <hartmans> But such statements need to be about factors to consider, with lots of room for interpretation.
18:54:42 <aba> for 3) I think iwj statement is good.
18:54:50 <marga> I don't
18:54:51 <hartmans> aba: I don't.
18:55:04 <marga> keithp, Debian is not governed by case law
18:55:07 <dondelelcaro> keithp: though there is a difference between dicta and non-dicta even in those decisions
18:55:08 <OdyX> hartmans: sure. But I'd like to see the urge, for the project, though.
18:55:09 <aba> keithp: nobody says debian works like caselaw,
18:55:10 <hartmans> iwj's statement has way too little room for interpretation and is way too focused on upstream
18:55:17 <Ganneff> imo "browserified" stuff is like a binary. how its generated i dont care (upstream way / tools or an own way), but the input needs to be there. so if its not -> rc.
18:55:19 <aba> marga, hartmans: why?
18:55:59 <aba> Ganneff: agreed (unless it's the only thing that exists, then it may be the source as well, but we had that with other binaries as well)
18:56:17 <hartmans> I think that being able to interact with upstream is only one of the factors; I think the interesting thing is not whether our users can interact with upstream, but rather, whether they can make the modifications they want to make.
18:56:22 <Mithrandir> we need the Debian version of the man on the Clapham omnibus.
18:56:33 <aba> hartmans: ok. I think the spirit looks mostly good to me
18:56:45 <keithp> aba: see also flight of the amazon queen
18:56:47 <marga> aba, it gives way too much importance to upstream.  There are plenty of cases when we can disagree with upstream.
18:56:49 <hartmans> Sometimes we'll disagree with upstream about what preferred form is.  I do think upstream is an important consideration but not nearly as strong as Jonas and iwj make it out to be.
18:56:58 <aba> marga: sure, it's not only upstream
18:57:20 <aba> basically, we want to be able (and our users to be able) to change whatever they want.
18:57:27 <Ganneff> preferred form of modification == the format in which patches are most likely to get accepted
18:57:32 <hartmans> aba: yes.
18:57:33 <Mithrandir> hartmans: I (somewhat surprisingly maybe) mostly agree with you.  There are multiple definitions of what source can reasonably mean.
18:57:44 <hartmans> Although please note there's complexity even there.
18:57:49 <aba> sure.
18:57:53 <Ganneff> if someone can prove to me they love to edit minified files, so be it
18:58:04 <hartmans> is a png really source for an image?  Is an ogg or mp3 source for audio?  what about a flac?
18:58:09 <Mithrandir> Ganneff: so if upstream only accepts patches tla diffs, nothing but a tla repo is source?
18:58:28 <hartmans> If I give you the flac for a mashup I've done rather than the nama project it's going to be a lot harder for you to make certain  modifications that me.
18:58:43 <Mithrandir> but this is a very deep rabbit hole.  I love to have the discussion, but I don't think we'll finish tonight if we go down it.
18:58:46 <keithp> dondelelcaro: my idea here is that the notion of 'source' is squishy enough that we should work on developing enough source/not-source instances that general patterns form
18:58:49 <hartmans> However, there's a balace here because we don't want to disallow all the icons and sounds in the archive.
18:58:54 <hartmans> and then there's perl and metaconfig.
18:58:58 <aba> Mithrandir: if you replace "upstream" by "reasonable developers" I think we're done
18:58:59 <helmut> hartmans: I note that perl fails your "whether they can make the modifications they want to make" test.
18:59:23 <Mithrandir> aba: hence my cry for our version of the man on the Clapham omnibus.
18:59:46 <hartmans> Anyway I'm trying to summarize this all and will have something to send out for people to look at.
18:59:54 <keithp> aba: I'd agree if you used 'debian developers'
18:59:55 <Mithrandir> helmut: without having looked at our patched perl, maybe we need to adjust practices.  It's not the end of the world.
19:00:13 <hartmans> Keith, I'd originally proposed that the submitter talk to ftp master directly if they wanted.  if you like I can propose mediation with the Tc as an additional option.
19:00:15 <Mithrandir> (https://en.wikipedia.org/wiki/The_man_on_the_Clapham_omnibus)
19:00:18 <aba> keithp: you could agree then
19:00:29 <keithp> this shouldn't be about what people outside the project think is source, it should be about what *we* think is source
19:00:50 <OdyX> For me, the questions are a) do we still need to take any sort of decision on libjs-handlebars (I think not); b) is a more general statement needed / wanted from the TC (I think not either).
19:00:53 <aba> we or our users, so "people closely related to debian"
19:01:02 <aba> a) - no.
19:01:03 <Mithrandir> keithp: I think we should at least give a nod to upstream's wishes.
19:01:30 <Mithrandir> keithp: if upstream's willing to accept patches in a given format, that probably makes that source, even if we'd not usually consider it that, I think.
19:01:35 <helmut> Mithrandir: sure, I just dislike putting different measures at different packages and I'm glad we stopped doing that for firmware.
19:01:42 <Mithrandir> (without having fully thought through it, mind)
19:01:45 <hartmans> odyx: a) I think we should give the submitter advice on how to proceed.  I think that unless we offer mediation that advice is not contraversial: you can talk to ftp if you like.
19:02:08 <keithp> Mithrandir: that might affect what we thought was source, but we should be willing to disagree -- upstream thinks that firmware is source in many projects
19:02:19 <aba> Mithrandir: if they're not unreasonable picky, e.g. only bitkeeper
19:02:47 <Mithrandir> aba: you misunderstand me, it's a positive requirement, so it'd be sufficient, but not necessary.
19:02:50 <hartmans> But yes, it would be useful to get a feel on whether we need a resolution here
19:02:59 <hartmans> or whether we just close with some comments thrown in.
19:03:25 <Mithrandir> I think it'd need a bunch of discussion more, since we're not aligned (yet)
19:03:30 <aba> Mithrandir: ok. yes, I mostly agree to it.
19:03:49 <OdyX> Okay. Anyone championing this ?
19:03:50 <Mithrandir> keithp: do they actually take patches to that form?
19:04:00 <marga> One of the many things I've been thinking is that when the DFSG were crafted, the "challenge" was towards copyright and so they are mostly focused on copyright and give a lot of detail of what is free or not regarding the copyright point of view.  Regarding source code, they only say that source code needs to be available. That's it.  I believe we are now at the point where the project needs to really think about what we mean by source code i
19:04:01 <marga> n many different instances. I don't think it's the TC's job to take this decision, although we could try to facilitate the discussion.
19:04:02 <hartmans> Mithrandir:  Yes, it seems like we'd need discussion to get to a resolution
19:04:09 <OdyX> #save
19:04:11 <aba> (basically this discussion has so many small loopholes that I'm happy if we agree to usual 95% of it, and consider the edges as "dangerous area")
19:04:26 <hartmans> I'd prefer for us to close without a vote or to have that discussion.
19:05:00 <Mithrandir> marga: I'm not sure if it is our job or not, tbh.
19:05:22 <hartmans> I'm happy to champion if we want to go forward
19:06:01 <OdyX> hartmans: what's your condition ? okay to champion if we're having the larger discussion ?
19:07:04 <hartmans> yes.  I'm also happy to be the one who quickly closes
19:07:11 <OdyX> #agreed hartmans to champion #830978 about browserified JS.
19:07:12 <hartmans> if we do that
19:07:25 <OdyX> #topic New member selection process
19:07:30 <hartmans> but I'd like to roughly figure out now if we're closing or trying to go forward.
19:07:31 <OdyX> (moving on, or we never finish)
19:07:59 <hartmans> Ok, I'd prefer that if you move on without finding a basic direction you get someone else to handle it.
19:08:22 <OdyX> #topic #830978: Browserified javascript and DFSG 2
19:08:34 <OdyX> Let's get that discussion through then.
19:08:43 <hartmans> VWho would like to try and see if we get anywhere with broader discussion?
19:08:48 <hartmans> We may of course fail.
19:09:30 <OdyX> I'm more in favour of issuing a statement on the precise case we _saw_, than a generic statement about upstream requirements / browserification / etc.
19:09:32 <Mithrandir> I would like to know what we're being asked to answer.  Is it the "is libjs-handlebars free?" question, or is it something else?
19:10:07 <marga> I think that this particular bug should be closed as by now there is basically no doubt for the course forward.  We may want to open a separate one to think more broadly about the general thing.
19:10:07 <ScottK> Since step 1 of "How to refer a question to the technical committee" was never done in this case, I think anything other than closing the bug just rewards bad behavior.
19:10:18 <hartmans> Mithrandir:  Trying to come up with a resolution
19:10:29 <OdyX> I'm aligned with both marga  & ScottK  here.
19:10:52 <hartmans> Okay, seeing no objections how about I close and suggest that after fixing the parser issue they can talk to ftp if they like
19:11:11 <aba> sounds reasonable, yes
19:11:15 <Mithrandir> wfm
19:11:28 <hartmans> okay, glad we had this: that's totally the opposite direction I thought we were going in.
19:11:44 <keithp> sounds good
19:11:47 <OdyX> ack
19:11:48 <Ganneff> ++
19:12:31 <OdyX> #agreed hartmans to close #830978 and suggest talking to FTP Masters if they see fit.
19:12:35 <OdyX> #topic New member selection process
19:13:18 <OdyX> I've been slacking here. We (aka anyone of us) should be sending a d-d-a mail with the schedule.
19:14:29 <OdyX> Anything to add ?
19:14:30 <marga> What needs to get done?
19:14:38 <OdyX> (75 minutes in)
19:14:49 <marga> Just send the schedule?
19:14:59 <Mithrandir> does anybody have complaints about the proposed schedule?
19:15:04 <Mithrandir> if not, we should just send it out.
19:15:19 <OdyX> yeah, acknowledging our agreement, and sending the schedule to the projec.t
19:15:31 <hartmans> At the closed meeting we seemed fairly agreed that our current process is not going to get a diverse TC
19:15:45 <hartmans> And discussed how we wanted to look at revisiting that for this round.
19:16:08 <hartmans> This month may not be the best time though.
19:16:19 <hartmans> How about we take this to list.
19:16:24 <hartmans> and send out the schedule
19:16:37 <OdyX> we need to involve the project for the diversity increase.
19:16:37 <keithp> agreed, I'm in another meeting now...
19:16:50 <OdyX> ack. Move that to list.
19:16:51 <aba> I think we should call it a meeting now
19:17:15 <OdyX> #topic Additional Business (varia)
19:17:55 <OdyX> I want to record that I misjudged the volunteers for a champion designation earlier this meeting. Sorry for that.
19:18:19 <OdyX> We see that when we're 1h+ in the meeting, we lose traction.
19:18:42 <Mithrandir> irc meetings are hard.
19:18:53 <dondelelcaro> I'm also in another meeting now, unfortunately
19:18:58 <marga> I'd drop the 5 minute wait at the beginning
19:19:03 <OdyX> if there's no other point, let's call it a meeting, and see us in a month. We all have enough action points to get done.
19:19:26 <OdyX> marga: we can try, sure.
19:19:39 <OdyX> Anyway. Have a nice end of day.
19:19:42 <OdyX> #endmeeting
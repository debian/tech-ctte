====================
#debian-ctte Meeting
====================


Meeting started by OdyX at 19:00:05 UTC. The full logs are available at
http://meetbot.debian.net/debian-ctte/2017/debian-ctte.2017-05-17-19.00.log.html
.



Meeting summary
---------------
* Round of names  (OdyX, 19:00:27)

* Review of previous meetings' TODOs  (OdyX, 19:04:05)

* #839172 TC decision regarding #741573 menu policy not reflected yet
  (OdyX, 19:04:34)

* #836127 New CTTE Members  (OdyX, 19:05:50)
  * ACTION: keithp to gather the names we have a post a ballot to the
    list.  (OdyX, 19:13:58)

* Additional Business  (OdyX, 19:14:23)

Meeting ended at 19:29:39 UTC.




Action Items
------------
* keithp to gather the names we have a post a ballot to the list.




Action Items, by person
-----------------------
* keithp
  * keithp to gather the names we have a post a ballot to the list.
* **UNASSIGNED**
  * (none)




People Present (lines said)
---------------------------
* OdyX (34)
* keithp (15)
* bremner (9)
* Mithrandir (6)
* fil (6)
* MeetBot (2)




Generated by `MeetBot`_ 0.1.4

.. _`MeetBot`: http://wiki.debian.org/MeetBot

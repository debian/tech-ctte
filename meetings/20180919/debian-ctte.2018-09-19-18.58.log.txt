18:58:35 <marga> #startmeeting
18:58:35 <MeetBot> Meeting started Wed Sep 19 18:58:35 2018 UTC.  The chair is marga. Information about MeetBot at http://wiki.debian.org/MeetBot.
18:58:35 <MeetBot> Useful Commands: #action #agreed #help #info #idea #link #topic.
18:58:39 * gwolf jumps quickly for a coffee
18:58:39 <bremner> ohai, just finished my last meeting
18:58:45 <marga> #topic Roll Call
18:58:51 * marga Margarita Manterola
18:58:52 <gwolf> (also "fresh" after a meeting)
18:58:58 * smcv Simon McVittie
18:58:59 * gwolf Gunnar Wolf
18:59:10 <marga> The topic action didn't work, though... hum
18:59:12 <OdyX> Didier Raboud
18:59:27 <fil> Philip Hands
18:59:32 <OdyX> (meeting not finished, will follow with just one eye)
19:00:27 <bremner> oh, I'm David Bremner, still
19:00:28 <gwolf> back
19:00:48 <marga> Let's see if topic changing works now
19:00:59 <marga> #topic Review of previous meeting AIs
19:01:49 <Mithrandir> Tollef Fog Heen
19:02:00 <marga> Yay. Ok. We had two AIs: to follow up on the two open bugs that we had.  We have followed up (albeit it took us some time). But I guess we can move on and discuss per bug.
19:02:12 <marga> #topic #904302 Whether vendor-specific patch series should be permitted in the archive
19:02:50 <marga> So, Mithrandir has a draft proposal.  I think we are basically ready to vote.
19:03:04 <marga> Or do we want to discuss more about it?
19:03:15 <Mithrandir> there's a typo in it which got pointed out, otherwise I think it's ready to go.
19:03:16 <OdyX> I haven't read the bug thread at all yet; I'll only vote if I manage to read it fully :(
19:03:34 <gwolf> I think we have all shown enough arguments. Save the typo already pointed out in the draft, I'd be happy if we voted on it
19:04:01 <bremner> I made my wording objection know, but it didn't seem to bother other people
19:04:24 <Mithrandir> I'll update the draft, give folks a couple of days, then call for a vote
19:04:26 <Mithrandir> sounds ok?
19:04:30 <bremner> sure
19:05:06 <marga> Ok for me
19:05:24 <OdyX> As long as you don't hold it on me; OK of course.
19:05:37 <Mithrandir> OdyX: I think we'll be enough without you too
19:05:37 <fil> well, I think we should probably let the Policy folks come up with whatever suits them to deal with the pre-Buster state (as mentioned in the bug) but I'm not that bothered either way
19:05:50 <Mithrandir> fil: they delegated the decision to us.
19:05:50 <marga> bremner, regarding your wording objection, I think it's clear from the scope of the decision that we are not ruling on whether different distros can have different source packages.  We can't rule on that.
19:06:14 <bremner> maybe I should comment further, because that's not what I meant at all
19:06:19 <Mithrandir> we can rule on what packages in the debian archive can build on downstream distributions. I don't think we want to, though.
19:06:44 <smcv> does anyone want there to be an option to vote on for "deprecate but do not forbid"?
19:06:45 <gwolf> That would be ridiculously overreaching
19:07:08 <marga> smcv, you mean have a "SHOULD NOT" forever?
19:07:20 <smcv> yes
19:07:27 <fil> Mithrandir: sure, but we can decide the result we want without telling them when to do uploads of different versions of policy (which we seem to be doing in the draft)
19:07:37 <smcv> (not saying I think it's a good idea...)
19:07:46 <marga> Given how little this is used, I'd rather we got rid of it completely. But I don't oppose having that as an option in the ballot.
19:08:29 <gwolf> OK, so draft should be updated accordingly...
19:09:25 <fil> does anyone think that's a good idea?  if not, there's not much point putting it in IMO
19:09:52 <smcv> not really, I just wanted to make sure we'd at least thought about it
19:10:11 <gwolf> fil: it would create a lower friction, and allow for packages to be buggy-but-not-RC almost forever if the maintainer strongly opposes this resolution
19:10:27 <marga> I think we should let Policy people decide when they are uploading changes. However, we are just saying "After Buster is released" which does not mean, one hour after, one day after or even one month after
19:10:30 <gwolf> But in my case, I would not vote for an eternal-SHOULD-NOT
19:11:04 <gwolf> marga: It just leaves them room to implement when ready.
19:11:07 <bremner> ftr, my wording objection to draft is that it can be read as encouraging debian contributors to make source packages that build differently, as opposed to making seperate source packages. I'm not convinced that's a good recommendation
19:11:28 <bremner> (build differently on different architectures)
19:11:44 <marga> s/architectures/distributions/?
19:11:50 <bremner> uh, right
19:12:01 <Mithrandir> if somebody writes up a proper proposal for smcv's strawman, I'll include it.  I'm not going to do it myself.
19:12:11 <gwolf> bremner: /methinks that if packages build different *following from the unpackaged source*, it's positive. That is, the build process can (will) be different...
19:12:17 <marga> It doesn't seem like anybody thinks that's a good idea.
19:12:44 <smcv> great, sorry for the distraction
19:13:01 <gwolf> I do not think bremner's argument will actually confuse people. And even so - We are being asked by Policy team for our opinion, and they will implement policy
19:13:14 <gwolf> So it's policy editors who would be confused or not. and I don't think they would :-]
19:13:23 <bremner> gwolf: I think that subissue is complicated and subtle and this bug doesn't require us to have an opinion on it
19:14:12 <bremner> but, it's not that major a point. It would not prevent me from supporting the draft (I think)
19:15:55 <Mithrandir> bremner: I think I see what you're thinking of.  Would including a "patch the package in a derivative distribution" in the list of options suffice?
19:16:29 <marga> bremner, I'm actually in favor of your diff, even if I don't think it makes much of a difference regarding having separate source packages.
19:17:12 <bremner> Mithrandir: it's an improvement, but I overall just rather not be definitive about the good ways of doing, we're only asked to rule on one way being bad
19:17:29 <Mithrandir> bremner: "such as", then?
19:17:37 <bremner> Mithrandir: sure.
19:18:12 <Mithrandir> I'll make a pass over it, let me know if it's sufficient
19:18:30 <marga> Ok, I think we are ready to set this as an action and move on
19:19:04 <marga> #action Mithrandir to incorporate changes into the current draft, send new draft for review and call for a vote after it's been reviewed.
19:19:16 <marga> #topic #904558 What should happen when maintscripts fail to restart a service
19:19:23 <Mithrandir> are folks happy with 2-3 days for review?
19:19:33 <marga> Yeah
19:19:38 <bremner> Mithrandir: fine for me, especially if one of those days is a weekend day
19:19:40 <marga> It's mostly done anyway
19:19:49 * gwolf agrees
19:20:23 <fil> sure
19:20:39 <marga> So, on this topic. My AI was to send a follow up email to the bug. I only got around to doing this on Monday :-/. This did generate a bit of traffic, but not much.
19:21:07 <bremner> IMHO, we need more discussion. Or at least I need more reading
19:21:11 <marga> Ian insists that it's a good thing for postinst to fail. Other people have mostly agreed that it causes unnecessary pain.
19:21:47 <bremner> it may come down to different use cases
19:21:59 <marga> Yeah
19:22:00 <bremner> (as far as why people have different views)
19:22:14 <marga> I agree, it does come down to different use cases.
19:22:27 <Mithrandir> I wrote a followup today, I don't know if people think I'm wrong or not.  Happy to have that discussion.
19:22:30 <smcv> Ian's point of view does seem to come from the idea that a Debian machine has a knowledgeable sysadmin who can recover from apt becoming sad
19:23:03 <Mithrandir> for me, it largely comes down about to what extent the APIs provided by postinst are upheld or not.
19:23:06 <smcv> which I'm sure was (had to be) true in dpkg's early history, but these days we aspire for Debian to be more universal than that
19:23:10 <Mithrandir> and those are on a per-package basis.
19:23:15 <bremner> Mithrandir: your reply is one thing I need to read more carefully, sorry
19:23:21 <marga> I come from a world where I have tens of thousands of users and I want their apt state to be healthy at all costs, as manually fiddling with a package for which postinst has failed is very expensive.
19:23:23 <gwolf> smcv: Right. A broken maint script is a dead end for desktop (or for that matter, any not-deeply-knowledgeable) user
19:23:48 <gwolf> I agree there are reasons where apt is justified in dying, and we must deal accordingly
19:23:57 <gwolf> But failing to restart a daemon is _not_ one of them
19:23:59 * smcv waves the "atomic upgrades" flag
19:24:03 <fil> I think anything that requires one to add 'exit 0' to maint/init scripts in order to progress is almost always a disaster -- I can imagine that there is a package somewhere that simply _must_ run it's deamon though, in which case perhaps it could be allowed to fail if the maintainer really thinks that's vital
19:24:15 <bremner> I feel like if we're establishing a technical concensus, that should happen on the bug, if possible
19:24:19 <marga> Mithrandir, I kinda agree, but there's still a lot of grayness into what it means for a package to uphold the API.
19:24:27 <Mithrandir> marga: I appreciate that, at the same time, to what extent can we fix that with better tooling?
19:24:43 <Mithrandir> marga: yes, "the API" is… not well defined.
19:24:52 <smcv> the daemons that seem closest to being API are things like dbus that lots of other packages depend on
19:25:01 <marga> Mithrandir, I'm not sure what you mean, fixing with better tooling.
19:25:08 <marga> I think we could stop recommending set -e
19:25:14 <smcv> (not that dbus-daemon supports being restarted anyway so maybe that's a bad example)
19:25:36 <marga> And recommend that postinst only fail in explicit situations when configuring dependencies of that package would be bad or impossible
19:25:50 <gwolf> marga: I don't know if I'd suggest dropping the set -e
19:25:52 <Mithrandir> (rdepends, hopefully)
19:26:00 <smcv> gwolf: I was about to say the same
19:26:06 <fil> smcv: and if it did, would the postinst failing when it didn't be likely to improve anyone's day?
19:26:10 <marga> Yes, rdepends.
19:26:24 <gwolf> But maybe because I think it forces maintainers to think on writing safe, foolproof scripts (or refrain from scripting at all, even better!)
19:26:36 <Mithrandir> I think getting away from shell scripts for maint scripts would be great, but that's exploding this entire issue. :-)
19:26:38 <marga> Why not? (I'm willing to be convinced, I want to know why you think that's not a good idea)
19:26:51 <smcv> for me, set -e is about changing shell's default "carry on regardless, unless you take steps to fail" into something more like try/except
19:27:33 <smcv> the analogy isn't perfect but the general idea is the same as exceptions: things unexpectedly going wrong break out of the normal control flow
19:27:46 <marga> gwolf, I don't think people using set -e are forced to write safe maintscripts.  This is just how it's always been done and people just blindly follow that, and then they have typos or bad assumptions in their scripts that force users to go add an exit 0 in between.
19:28:15 <smcv> under set -e, the default for things where you haven't thought "what if this fails? can it fail?" is to abort
19:28:32 <smcv> without set -e, the default is to carry on as though everything was fine
19:28:35 <gwolf> marga: In a way, maint scripts are written with the same mindset as makefiles, which operate with a set -e mindset
19:28:43 <marga> bremner, I don't think we will decide anything here today, but having these discussions helps understand the nuances better.
19:28:44 <smcv> (except for the last statement in the script, which sets the exit status)
19:28:48 <bremner> it's quite hard to reason about shell script behaviour without set -e
19:29:20 <bremner> I mean, when I'm on line 100, how many of the previous things failed?
19:29:34 <marga> gwolf, but they are used in a very different context. A Makefile is called on purpose and the Makefile failing just means the packages doesn't build, not that the whole machine might be rendered unusable.
19:30:03 <smcv> I don't think merrily carrying on with execution of the maintscript after a failure is the right answer either
19:30:15 <smcv> I mean, it's fine to have a lot of || true
19:30:29 <gwolf> marga: but maintscripts can be seen as some fashion (again, not-completely-perfect parallel) of makefiles of a sort. You want to reach a target. Failing steps mean, well, the target cannot be reached (without operator intervention)
19:30:31 <smcv> that means someone has thought "what if this fails?" and concluded that the right answer is "carry on"
19:30:37 <marga> Yeah, but then you have a typo in your if statement and your || true doesn't catch it...
19:30:42 <Mithrandir> marga: I think that any point in a postinst dying and leaving the unusable is a critical bug.
19:30:47 <gwolf> (Of course, we all want operator intervention to be almost never needed)
19:31:19 <gwolf> marga: Then it would be an über-RC bug in the first place! Catch it before it reaches Testing!
19:31:27 <fil> it certainly encourages bug reports for the scripts to fail in the case of typo, which is a good aspect of this
19:31:37 <smcv> do we all agree that the best maintscript is the one that is ENOENT?
19:31:44 <bremner> I do
19:31:47 <gwolf> yup
19:31:52 <OdyX> sure
19:31:58 <fil> yes
19:32:02 <Mithrandir> smcv: failing that, does not have #! /bin/sh as the shebang. :-P
19:32:50 <smcv> I think that might be a lost cause, but I appreciate the intention
19:32:56 <marga> Yes
19:33:12 <marga> (to the above question)
19:33:56 <marga> Anyway, I guess I can give up my quest for set +e. But I do think we should recommend to avoid failing except on the rare case when it absolutely must fail because rdeps won't be able to get configured correctly
19:34:42 <smcv> Ian does have a point about it being important to be able to tell that something is wrong
19:35:15 <marga> No, I disagree with that. Breaking the package management system is not a good way to tell if something is wrong.
19:35:19 <smcv> but I think that's syslog/the journal, rather than dpkg leaving apt in a confused state
19:35:44 <marga> I do agree that if rdeps postinsts are going to break because of the package not being correctly configured, that is a valid reason for postinst to fail.
19:35:54 <bremner> marga: to be fair, I think your definition of "breaking the package management system" has some assumptions built in
19:35:56 <marga> Ah, ok, then I agree with you :)
19:36:00 <gwolf> Many systems' logs/journals are never ever checked.
19:36:03 <smcv> (or even systemctl; or nagios and friends on more "managed" systems)
19:36:18 <Mithrandir> bremner: yeah, I disagree with the package system being broken by a failed postinst.
19:36:43 <bremner> I mean, it's a bad state, but lets be careful with our terminology
19:37:01 <Mithrandir> nod
19:37:15 <marga> Well, it's in a state that might not let you install other packages that you might need to install to remedy the problem
19:37:19 <smcv> Ian asserts that it's an apt bug that it doesn't let you do anything while unconfigured packages exist
19:37:30 <marga> That's what I call broken, but I can agree to call it something else.
19:38:02 <marga> Well, I don't think that statement is correct. It sometimes lets you do some things. It depends on the dependency graph
19:38:11 <bremner> is installing packages usually the answer to broken postints?
19:38:27 <marga> Sometimes.
19:38:30 <Mithrandir> yes, your navigation room is constrained if you have a half-configured package.
19:38:48 <Mithrandir> s/room/space/
19:38:52 <gwolf> bremner: you cannot install tools to help you out of the hole
19:39:09 <gwolf> (welllllll... you kind-of-can, but it requires you to be creative and know your way)
19:39:15 <Mithrandir> gwolf: yes, you can, it just takes careful navigation. :-P
19:39:19 <jcristau> bremner: if that package is $editor
19:39:22 <bremner> gwolf: yeah, that's my point
19:39:46 <bremner> sed is priority required
19:39:50 <fil> it strikes me that using the failure of a script as the method to tell the user something in this case is a bit like telling a car driver that they are low on fuel by having the wheels fall off.
19:40:07 <marga> lol
19:40:14 <gwolf> bremner: (hmmm, your version of sed looks *very* much like Emacs to me! Have you scripted it?)
19:40:14 <bremner> anyway, I want to distinguish between "hard for most people" and "impossible".
19:40:22 <Mithrandir> fil: we really should have a better notification mechanism. Now, if somebody were to create that…
19:40:26 <gwolf> (yes, I snooped on your monitor)
19:40:39 <marga> bremner, well, it depends on which postinst broke.
19:40:49 <marga> But, anyway, does it matter?
19:41:06 <gwolf> "Technical committee does not do detailed design work" :-]
19:41:51 <bremner> marga: words like "broken" tend to be emotionally laden in Debian
19:42:10 <marga> Can we agree on "in an undefined state" and move on?
19:42:33 <Mithrandir> it's defined.  Half-configured.
19:43:00 <Mithrandir> the exact semantics depend on the package, just like what the provided API is for a package.
19:43:11 <Mithrandir> (when successfully configured)
19:43:23 <marga> The package is in Failed-Config. What's undefined is what apt will or will not let you do with the rest of your system.
19:43:51 <marga> That's why the system (not the package) is in an undefined state
19:44:54 <Mithrandir> I disagree with it being undefined; but I don't also think we'll make forward progress in an IRC meeting now.
19:45:15 <marga> So, let's try to get back on track. We were asked to specifically rule about what should maintscripts do when (re)starting a service fails.  The question didn't even include whether the service in question is provided by the package or not.
19:45:34 <bremner> that's implicit IMHO
19:45:42 <bremner> policy is concerned with the contents of packages
19:46:03 <bremner> oh, "the" package, sorry
19:46:23 <Mithrandir> I don't think packages should randomly restart services from other non-coordinating packages.
19:47:07 <smcv> do we all think the answer is: in general the maintscript should "succeed" anyway, but there are exceptional cases (where the daemon is relied on by dependent packages) where it might be better for it to fail?
19:47:36 <bremner> smcv: I don't know
19:47:40 <Mithrandir> yes, my example was SSH
19:47:44 <marga> smcv, I'd be comfortable with something like that.
19:48:17 <fil> smcv: with the exception of SSH, yes
19:48:40 <marga> fil, what would the exception look like?
19:48:49 <gwolf> Exceptions apply, but they are usually widely enough understood
19:48:58 <marga> I mean, Simon's statement already gives room for exceptions.
19:49:13 <bremner> are those the only exceptions?
19:50:00 <smcv> I didn't intend to say "relied on by dependent package" was the only valid exception, but looking again at what I wrote I can see that that's unclear
19:50:01 <Mithrandir> I would leave that up to maintainers.  "This is the default, if you have good reasons, you can diverge"
19:50:15 <smcv> yeah, that
19:51:16 <marga> I'd like to say that the exceptional cases should be related to rdeps
19:51:37 <Mithrandir> marga: does that mean you don't think ssh is a valid example?
19:51:41 <marga> Because otherwise, maintainers are going to feel entitled to be an exceptional case "to make the user pay attention".
19:52:08 <gwolf> marga: I do not believe that's a common mindset between our maintainers
19:52:11 <marga> Mithrandir, can you elaborate on the scenario?
19:52:36 <fil> marga: I think ssh needs to make sure that you know if e.g. the config format changed, and you neglected to accept the new config file, such that it no longer runs -- so I'd expect ssh to be an exception even though it isn't because of dependent packages (at least until we come up with a better alert mechanism)
19:52:57 <smcv> I assume the scenario is: I am logged in over ssh, I upgrade sshd, it fails to restart, I log out, oops! I can't get back in
19:53:03 <marga> How does it help you to have ssh in Failed-Config?
19:53:12 <bremner> you won't reboot?
19:53:15 <Mithrandir> marga: (strawman): you have a setting in sshd config which is not supported by a newer openssh upstream, and you run with DEBIAN_FRONTEND=noninteractive.  You upgrade the package, ssh is attempted to restart, it fails due to said setting.
19:53:20 <jcristau> marga: if apt exits non-zero you have a chance to notice
19:53:28 <jcristau> before you log out
19:53:38 <marga> This is all assuming you are looking at the output
19:53:44 <marga> If ssh updated through automatic updates
19:53:51 <marga> How does it help you that it's in Failed-Config?
19:53:51 <jcristau> no, it's assuming you're looking at the exit code, which is all you have
19:54:30 <bremner> upgrading ssh through automatic upgrades might be a bad idea
19:54:42 * jcristau thinks this should keep applying to all services but it seems like the TC disagrees
19:55:01 <bremner> at least for machines you don't have physical access to
19:55:21 <marga> bremner, it's not like you can decide what packages get updated automatically when you setup automatic updates
19:55:29 <bremner> uh, yes?
19:55:32 <smcv> devil's advocate: so might not upgrading a network-facing service that tends to be let through firewalls?
19:55:44 <bremner> marga: sorry, was that serious or sarcasm?
19:56:02 <marga> Sorry, I might use a different automatic upgrading mechanism that doesn't have that level of granularity
19:56:02 <gwolf> jcristau: Ideally every sysadmin should carefully consider every action they take with root powers
19:56:07 <gwolf> That happens not to be the case
19:56:55 <bremner> marga: on servers I only take security updates automatically. I concede that ssh might be such. And then I might be screwed
19:57:21 <marga> Ah, ok, I see what you mean, I thought you meant there was a way to tell automatic updates to not update ssh in particular.
19:57:33 <Mithrandir> I think breaking upgrades are much more likely to be across suites than just applying security upgrades.
19:57:53 <jcristau> marga: there... is?
19:58:03 <jcristau> // List of packages to not update (regexp are supported)
19:58:04 <jcristau> Unattended-Upgrade::Package-Blacklist {
19:58:10 <Mithrandir> marga: just pin it?
19:58:17 <Mithrandir> or what jcristau says
19:58:34 <marga> Ok, you can pin it or blacklist it... Do you actually do that?
19:58:40 <jcristau> gwolf: i don't get your point sorry.
19:59:08 <OdyX> Security upgrades should really really not fail ever; and if a configuration changes, the default should really be to ensure safe restart
19:59:25 <marga> I think both Gunnar and me are thinking about users that are not sysadmins.  That install packages through graphical interfaces and then are very unhappy when they need to fix a broken postinst
19:59:31 <gwolf> jcristau: You argue that it'd be better if all services were only restarted due to explicity sysadmin action
19:59:47 <jcristau> gwolf: i don't think i'm saying that, no
19:59:48 <gwolf> There are many reasons to special-case ssh in this reasoning
19:59:57 <gwolf> oh, that's what I read from your "/me"
20:00:05 <jcristau> i
20:00:06 <Mithrandir> OdyX: fwiw, we've had a upstream version bump for ssh in a security upgrade in living memory.
20:00:25 <Mithrandir> marga: I don't think a full /boot is a broken postinst.
20:00:26 <jcristau> i'm saying when things fail i want to know
20:00:39 <marga> Mithrandir, what?
20:01:04 <Mithrandir> marga: /boot is full, you install a new kernel, it fails to install.  You claim that's a broken postinst
20:01:08 <Mithrandir> I don't think it is.
20:01:20 <OdyX> Mithrandir: in such a case, I trust SSH to either have restarted because the conffile was still default, or not restarted because it was…
20:01:24 <Mithrandir> or we're talking about completely different cases.
20:01:24 <marga> I don't claim such a thing
20:01:28 <gwolf> jcristau: Yes. I also want to know if something fails in my server, even in my desktop. But I am far from a common user
20:01:46 <Mithrandir> marga: you say that "postinst should never fail"; what should then the postinst do in that case?
20:01:52 <gwolf> FWIW, I just checked - my systemd says "state:degraded" for my desktop, and I haven't bothered to care...
20:02:11 <gwolf> jcristau: would it be OK for apt to be unusable if pulseaudio failed to restart?
20:02:32 <jcristau> imo, yes
20:02:54 <marga> Mithrandir, a full /boot leads to unhappiness, with or without a postinst failing.  The postinst failing does not help the unhappiness
20:03:12 <smcv> or since pulseaudio as a system service is frowned upon anyway, s/pulseaudio/openarena-server/?
20:03:40 <gwolf> in my case - yes, my desktop currently says cgmanager is failed. I will notice when/if I try to next run lxc. For *many* users, many services that are only seldom used are installed
20:03:42 <jcristau> marga: a failing postinst gives you a chance to fix it before rebooting to a missing initramfs?
20:04:08 <Mithrandir> jcristau: it also means that dpkg --configure -a will actually build a working initramfs
20:04:15 <Mithrandir> (assuming free disk space)
20:04:19 <jcristau> right
20:04:23 <marga> jcristau, only if you are looking at it.  Which if you are could also work by you seeing the error message.
20:04:31 <marga> The configure argument is more convincing there
20:04:31 <jcristau> marga: nope.
20:04:52 <jcristau> terminal output in an update is something i never ever see unless it exits non-zero
20:05:29 <marga> So, we circle back to be able to raise error messages better.
20:05:58 <jcristau> e.g. because i'm running https://salsa.debian.org/dsa-team/mirror/dsa-misc/blob/master/scripts/multi-tool/One-debian-upgrade on 200 machines at once
20:05:59 <marga> I really don't think that failing postinst is the right way, because in this time, a lot of people are not seeing it. It just happens in the background.
20:06:19 <jcristau> a failing postinst is the only mechanism we have
20:06:30 <Mithrandir> I think that for the cases of having thousands of users and ensuring that upgrades never fail, you want to do it differently; by doing a/b deployments of images and such.
20:06:30 <marga> Sure, I'm running debian upgrades on 100k machines at the same time. I want their postinsts not to fail.
20:07:31 <smcv> so the question is: which is the lesser evil
20:07:39 <marga> I think we have learned things from this discussion, but now we need to ponder these issues some more and follow up on the mailing list.
20:07:45 <marga> What would be the action item?
20:07:49 <smcv> losing the information that it failed
20:08:09 <smcv> or causing apt to be in a difficult-to-use state?
20:08:22 <bremner> it depends (TM)
20:08:25 <smcv> obviously, neither is good
20:09:51 <Mithrandir> I need to pop out RSN.
20:10:39 <marga> Yeah, me too
20:10:47 <marga> Can we agree on a todo for this?
20:10:54 <bremner> does someone want to try to summarise the bug thread before the next meeting?
20:11:02 <bremner> that seems like a minimal steop
20:11:03 <gwolf> The discussion is interesting, but I believe we won't walk beyond a moderate-sized circle
20:11:39 <Mithrandir> can we at least agree about the tradeoffs we're making in the recommendation?
20:11:49 <bremner> hopefully.
20:12:08 <Mithrandir> I don't necessarily think we'll _agree_ about what the right tradeoffs are, but at least that "if we optimise for X, Y gets treated worse" and so on.
20:12:09 <gwolf> I cannot volunteer to summarize - I am facing a very busy couple of weeks
20:12:20 <marga> I sent the previous summary and it was biased by my hate of failing-postinsts... Can someone else send the next summary?
20:12:49 <marga> smcv, ?
20:13:08 <smcv> ok, I'll see what I can do
20:13:40 <marga> #action smcv to summarize the current state of the discussion to the bug
20:13:59 <marga> Alright, and with that, I'd move to endmeeting unless someone has something really urgent.
20:14:30 <bremner> my urgent thing involves leaving ;)
20:14:37 <marga> kk
20:14:40 <marga> #endmeeting
#!/bin/sh

origdate=$1
if [ -z "$origdate" ]; then
  origdate="now"
fi

YEAR=$(date +%Y "-d $origdate")
YMD=$(date +%Y%m%d -d "$origdate")
YMD2=$(date +'%Y-%m-%d' -d "$origdate")

echo Obtaining meeting notes for $YMD2

set -x
mkdir -p ${YMD};
(cd ${YMD};
    wget -nd -np -r -l 1 -A "debian-ctte.$YMD2-*.txt" http://meetbot.debian.net/debian-ctte/${YEAR}/;
)

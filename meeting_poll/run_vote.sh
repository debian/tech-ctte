#!/bin/sh

echo "January meeting"
../scripts/pocket-devotee \
    --option 'A: Tuesday 16:00 UTC (January 24th)' \
    --option 'B: Tuesday 17:00 UTC (January 24th)' \
    --option 'C: Tuesday 18:00 UTC (January 24th)' \
    --option 'D: Tuesday 19:00 UTC (January 24th)' \
    --option 'E: Tuesday 20:00 UTC (January 24th)' \
    --option 'F: Wednesday 16:00 UTC (January 25th)' \
    --option 'G: Wednesday 17:00 UTC (January 25th)' \
    --option 'H: Wednesday 18:00 UTC (January 25th)' \
    --option 'I: Wednesday 19:00 UTC (January 25th)' \
    --option 'J: Wednesday 20:00 UTC (January 25th)' \
    --option 'K: Thursday 16:00 UTC (January 26th)' \
    --option 'L: Thursday 17:00 UTC (January 26th)' \
    --option 'M: Thursday 18:00 UTC (January 26th)' \
    --option 'N: Thursday 19:00 UTC (January 26th)' \
    --option 'O: Thursday 20:00 UTC (January 26th)' \
    --option 'Z: Further Discussion' \
    --default-option 'Z' \
    --quorum 5 \
    << EOF
hartmans: C = D = E = K = L = M = N > A = B = G = H = I = J > F = O > Z
odyx: B = C = D = G = H = I > A = F > E = J > K = L = M = N = O > Z
keithp: A = B = C = D = F = G = H = I = L = M = N = O > E = J = K > Z
tfheen: A = F = K > B = G = L > Z > C = D = E = H = I = J = M = N = O
philh: C = D = H = I = M = N > B = E = G = J = O = L > A = F = K > Z
marga: C = D = E > B = I = J > K > L > O > M = N > Z > A = F = G = H
EOF

echo "February meeting"
../scripts/pocket-devotee \
    --option 'A: Tuesday 16:00 UTC (February 21st)' \
    --option 'B: Tuesday 17:00 UTC (February 21st)' \
    --option 'C: Tuesday 18:00 UTC (February 21st)' \
    --option 'D: Tuesday 19:00 UTC (February 21st)' \
    --option 'E: Tuesday 20:00 UTC (February 21st)' \
    --option 'F: Wednesday 16:00 UTC (February 22nd)' \
    --option 'G: Wednesday 17:00 UTC (February 22nd)' \
    --option 'H: Wednesday 18:00 UTC (February 22nd)' \
    --option 'I: Wednesday 19:00 UTC (February 22nd)' \
    --option 'J: Wednesday 20:00 UTC (February 22nd)' \
    --option 'K: Thursday 16:00 UTC (February 23rd)' \
    --option 'L: Thursday 17:00 UTC (February 23rd)' \
    --option 'M: Thursday 18:00 UTC (February 23rd)' \
    --option 'N: Thursday 19:00 UTC (February 23rd)' \
    --option 'O: Thursday 20:00 UTC (February 23rd)' \
    --option 'Z: Further Discussion' \
    --default-option 'Z' \
    --quorum 5 \
    << EOF
hartmans: C = D = E = K = L = M = N > A = B = G = H = I = J > F = O > Z
odyx: G = H = I = L = M = N > F = K > E = J = O > Z > A = B = C = D
keithp: A = B = C = D = F = G = H = I = L = M = N = O > E = J = K > Z
tfheen: B = C = D = E = G = H = I = J = L = M = N = O > A = F = K > Z
philh: C = D = H = I = M = N > B = E = G = J = O = L > A = F = K > Z
marga: C = D = E > B = I = J > K > L > O > M = N > Z > A = F = G = H
EOF
